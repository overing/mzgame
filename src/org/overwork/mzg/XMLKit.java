package org.overwork.mzg;

import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 *
 * @author overing
 */
public class XMLKit {

    private static final DocumentBuilder buider;

    static {
        DocumentBuilder _buider = null;
        try {
            _buider = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
        buider = _buider;
    }

    public static Document createXMLDoc() {
        return buider.newDocument();
    }

    public static String xmlDoc2String(Document doc) {
        try {
            StringWriter space = new StringWriter();
            Transformer transformer;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "no");
            transformer.transform(new DOMSource(doc), new StreamResult(space));
            return space.toString();
        } catch (TransformerConfigurationException ex) {
            ex.printStackTrace();
        } catch (TransformerException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Document string2XML(String str) {
        try {
            Document document = buider.parse(new ByteArrayInputStream(str.getBytes()));
            return document;
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * 取得 Node Attrib 的 Value 捷徑
     * @param node Node 參考
     * @param attribName Attrib 名稱
     * @return Attrib 的 Value
     */
    public static int getAttribValue(Node node, String attribName) {
        if (node == null || node.getAttributes() == null) {
            return -1;
        }
        node = node.getAttributes().getNamedItem(attribName);
        attribName = node.getNodeValue();
        return Integer.parseInt(attribName);
    }
}
