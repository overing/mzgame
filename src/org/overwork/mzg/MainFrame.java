package org.overwork.mzg;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.util.Vector;
import javax.swing.*;
import org.overwork.mzg.maze.*;
import org.overwork.mzg.connection.*;

/**
 * MainFrame 類別是整個遊戲的主程式以及負責控制 {@link Maze} 類別<br />
 * 以及定義一些狀態控制常數。這個類別繼承JFrame類別。
 * @author Overing Zero
 * @see Maze
 * @see javax.swing.JFrame
 */
public class MainFrame extends JFrame implements Runnable {

    /** 程式版本號 (從2009/01/27開始寫的) */
    public static final String VERSION = "v091203 by Overing Zero";
    /** GAME_TITLE 常數定義視窗的標題以及遊戲標題畫面的標題。 */
    public static final String GAME_TITLE = "迷宮對戰遊戲";
    /** 畫面重繪延遲時間 */
    private static final long FREQUENCY = 30;
    /**"遊戲規則" 畫面要顯示的訊息 (load from file "rule")*/
    private static final String[] RULE_MESSAGE;
    /** "關於本程式" 畫面要顯示的訊息 */
    private static final String[] ABOUT_MESSAGE = {
        "程式名稱:" + GAME_TITLE,
        "作者名稱:Overing, 橋",
        "素材來源:",
        "     GNOME png icons",
        "     http://neortp.k-server.org/",
        "     http://sozaiyaex.web.fc2.com/",
        "程式版本:" + MainFrame.VERSION,
        "(可能不準確!到後來都忘了要改...XP)",
        ""
    };

    //靜態初始化區塊 (在"MainFrame.class"被載入到jvm的時候執行)
    static {
        //嘗試讀取規則說明檔內文到RULE_MESSAGE陣列
        String[] saTmp = new String[1];
        try {
            InputStream inputStream = MainFrame.class.getResourceAsStream("rule");
            BufferedReader input = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            Vector<String> list = new Vector<String>();
            while ((line = input.readLine()) != null) {
                list.add(line);
            }
            saTmp = list.toArray(saTmp);
            input.close();
            list.clear();
            list = null;
        } catch (Exception ex) {
            saTmp[0] = "(沒有找到規則說明檔)";
        }
        RULE_MESSAGE = saTmp;
    }

    //--------------------------------------------------------------------------
    /** State例舉 定義出 state 變數可以有的狀態值 */
    private static enum State {

        /** TITLE 常數定義狀態 遊戲標題畫面*/
        TITLE,
        /** LOCAL_TIP 常數定義狀態 單人模式遊戲的初始化 */
        LOCAL_TIP,
        /** LOCAL_PLAYING 常數定義狀態 單人模式遊戲執行中 */
        LOCAL_PLAYING,
        /** LOCAL_FINISH 常數定義狀態 單人模式完成遊戲 */
        LOCAL_FINISH,
        /** MULTI_INPUT_NAME 常數定義狀態 多人遊戲 命名遊戲畫面*/
        MULTI_INPUT_NAME,
        /** MULTI_MODE_CHOOSE 常數定義狀態 多人遊戲 模式選擇畫面*/
        MULTI_MODE_CHOOSE,
        /** MULTI_LIST_SERVER 常數定義狀態 多人遊戲 遊戲清單畫面*/
        MULTI_LIST_SERVER,
        /** MULTI_JOIN_FAIL 常數定義狀態 多人遊戲 加入失敗*/
        MULTI_JOIN_FAIL,
        /** MULTI_WAIT_START 常數定義狀態 多人遊戲 資料同步畫面*/
        MULTI_WAIT_START,
        /** MULTI_PLAYING 常數定義狀態 定義多人模式遊戲進行中 */
        MULTI_PLAYING,
        /** MULTI_FINISH 常數定義狀態 多人模式完成遊戲 */
        MULTI_FINISH,
        /** MULTI_ASK_EXIT 常數定義狀態 多人模式遊戲中途離開 */
        MULTI_ASK_EXIT,
        /** RULE 常數定義狀態 規則畫面 */
        RULE,
        /** ABOUT 常數定義狀態 關於本程式訊息 */
        ABOUT;
        /** 狀態夾帶的額外訊息 */
        String msg;

        private State setMsg(String string) {
            msg = string;
            return this;
        }
    }

    /**
     * 狀態管理器<p />
     * 所有的狀態改變都必須透過管理器進行<br />
     * 以管理所有的狀態變化以及呼叫相應的變數處理<br />
     * <br />
     * P.S.<br />
     * 本來沒這東西的, 因為發現程式碼裡面任意改變狀態的話<br />
     * 會因為沒有做好適當地初始化跟結束狀態處理造成許多預期外的狀況<br />
     * 所以設立這個類別
     */
    private class StateManager {

        /** 變數用來儲存現在的狀態, 作為繪圖與狀態改變的依據, 值可以是 State 例舉當中的項目 */
        private State state;

        /**
         * 構成一個狀態管理器
         * @param state 初始化的狀態
         */
        public StateManager(State state) {
            this.state = state;
        }

        /**
         * 欲改變狀態時必須呼叫此副程式進行改變<p>
         * 這副程式在改變狀態前會呼叫前置處理<br />
         * 改變狀態之後呼叫後置處理
         * @param newState 新的狀態
         */
        public synchronized void changeState(State newState) {
            State old = State.valueOf(state.name());
            stateChangePretreatment(state(), newState);
            state = newState;
            stateChangeHandling(old, state());
        }

        public synchronized State state() {
            return state;
        }
    }
    /** StateManager 狀態管理器*/
    private final StateManager sm;

    //--------------------------------------------------------------------------
    /** TitleMenu例舉 定義出 titleMenu 變數可以有的狀態值 */
    private static enum TitleMenu {

        LOCALGAME,
        MULTIGAME,
        RULE,
        ABOUT,
        EXIT;
        private static final String[] string = new String[]{
            "單機兩人遊戲",
            "連線多人遊戲",
            "遊戲規則",
            "關於本程式",
            "離開"
        };

        public String toString() {
            return string[ordinal()];
        }

        public void paint(Graphics g, int w, int h, Font font) {
            String ds;
            int hPos, vPos = calcVerticalSitePer(h, g, 0.5);

            for (TitleMenu t : values()) {
                if (this == t) {
                    g.setFont(font.deriveFont(Font.BOLD));
                    ds = "[" + t + "]";
                } else {
                    g.setFont(font);
                    ds = t + "";
                }

                hPos = calcHorizontalSitePer(w, g, ds, 0.5);
                vPos += (g.getFontMetrics().getHeight() * 1.5);
                g.drawString(ds, hPos, vPos);
            }
        }
    }
    /** titleMenu 變數用來儲存標題畫面游標所停留的項目, 值可以是 TitleMenu 例舉中的項目 */
    private TitleMenu titleMenu;
    //--------------------------------------------------------------------------

    /** MultiModeMenu例舉 定義出 multiModeMenu 變數可以有的狀態值 */
    private static enum MultiModeMenu {

        SERVER,
        CLIENT,
        BREAK;
        private static final String[] string = new String[]{
            "伺服器模式",
            "客戶端模式",
            "回標題畫面"
        };

        public String toString() {
            return string[ordinal()];
        }

        public void paint(Graphics g, int w, int h, Font font) {
            String ds;
            int hPos, vPos = calcVerticalSitePer(h, g, 0.5);

            for (MultiModeMenu t : values()) {
                if (this == t) {
                    g.setFont(font.deriveFont(Font.BOLD));
                    ds = "[" + t + "]";
                } else {
                    g.setFont(font);
                    ds = t + "";
                }

                hPos = calcHorizontalSitePer(w, g, ds, 0.5);
                vPos += (g.getFontMetrics().getHeight() * 1.5);
                g.drawString(ds, hPos, vPos);
            }
        }
    };
    private MultiModeMenu multiModeMenu;
    //--------------------------------------------------------------------------
    private Server server;
    private Client client;
    private static final int ClientNameLimit = 20;
    private String clientName = "";
    private Vector<String> serverList; //存放由client返回的server清單
    private int serverListSel; //儲存選擇的servre清單
    private int[] playersImageIndex; //存放所有玩家的選圖編號
    private String[] playersName; //存放所有玩家的名稱
    private int[] drawArray;
    //--------------------------------------------------------------------------
    private Font defFont; //預設字體
    private Maze maze; //用來儲存迷宮物件
    private long startTime, userTime; //儲存遊戲開始時間, 花費時間
    private BufferStrategy bufferStrategy; //繪圖緩衝策略(解決畫面閃爍)
    private BufferedImage backstage; //重繪時的背景畫布
    private KeyListener keyListener; //按鍵狀態偵測物件
    private boolean running; //主執行緒續行判別符號
    private volatile Thread mainThread; //主執行緒:負責畫面重繪, 按鍵檢測等
    private Player p0, p1; //玩家捷徑:local game使用兩者, multi game只使用 p0
    private int keyState;
    private static Color bc = new Color(255, 255, 255, 255), fc = new Color(255, 255, 255, 160);
    private int printRualShift = 0;
    /** debug mode switch */
    private boolean debug_MaskFlag;
    // ---------- 物件變數宣告結束 ---------- //

    /** 建立一個 MainFrame 物件 */
    public MainFrame(boolean fillScreen) {
        //初始畫狀態變數
        sm = new StateManager(State.TITLE);
        titleMenu = TitleMenu.LOCALGAME;
        multiModeMenu = MultiModeMenu.SERVER;
        serverListSel = -1;

        //設定視窗標題
        setTitle(MainFrame.GAME_TITLE + " (" + MainFrame.VERSION + ")");

        //設定視窗關閉時的動作
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        //生成按鍵處理物件＆註冊按鍵事件監聽
        addKeyListener(keyListener = new KeyListener(this));

        //禁止改變表單視窗大小
        setResizable(false);

        //禁用輸入法
        enableInputMethods(false);

        if (fillScreen) {
            //全螢幕模式開始
            DisplayManager.setFullScreen(this);//*/

            //隱藏滑鼠游標
            Image spIcon = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
            Toolkit tt = Toolkit.getDefaultToolkit();
            Cursor c = tt.createCustomCursor(spIcon, new Point(0, 0), "hide");
            setCursor(c);
        } else {
            //視窗模式開始
            DisplayManager.setWindowMode(this, 800, 600);//*/
        }

        //設定預設字型
        defFont = new Font(Font.SANS_SERIF, Font.PLAIN, getH() / 40);

        //呼叫圖片工具載入圖片
        ImageKit.readyL2L3Image(getH());

        //繪製角色選擇畫面
        ImageKit.drawPSBI(getW(),getH(), defFont);

        //建立緩衝策略 緩衝區數量2
        createBufferStrategy(2);

        //取得視窗的緩衝區 記憶體位置
        bufferStrategy = getBufferStrategy();

        //建立背景畫布 記憶體空間
        backstage = DisplayManager.createBufferedImage(getW(), getH());

        //生成＆啟動主執行緒
        (mainThread = new Thread(this)).start();
    }

    public String getClientName() {
        return clientName;
    }

    private void breakMULTI_MODE_SELECT() {
        if (sm.state() == State.MULTI_WAIT_START) {
            sm.changeState(State.MULTI_MODE_CHOOSE);
        }
    }

    public void c_SetDrawArray(int id) {
        drawArray = new int[4];
        for (int n = 0, i = 0; i < 5; i++) {
            if (i != id) {
                drawArray[n++] = i;
            }
        }
    }

    public void c_UpdatePlayerInfo(int pid, String pname, int imageNum) {
        if (playersImageIndex != null) {
            playersImageIndex[pid] = imageNum;
        }
        if (playersName != null) {
            playersName[pid] = pname;
        }
    }

    public void c_StartMultiGame() {
        sm.changeState(State.MULTI_PLAYING);
    }

    public int c_GetImageIndex(int id) {
        return playersImageIndex[id];
    }

    public void c_BreakMULTI_MODE_SELECT() {
        breakMULTI_MODE_SELECT();
    }

    public void c_FinishMultiGame(String data) {
        sm.changeState(State.MULTI_FINISH.setMsg(data));
    }

    /** 被狀態管理器所呼叫, 做一些狀態改變的前置處理(狀態前進) */
    private void stateChangePretreatment(State nowState, State newState) {
        if (nowState == newState) {
            //
        } else if (nowState == State.TITLE && newState == State.LOCAL_PLAYING) {
            debug_MaskFlag = false;
            maze = new Maze(16, 12);
            maze.newMap();
            p0 = maze.addPlayer(0);
            p1 = maze.addPlayer(1);
            maze.makeItems();
        } else if (nowState == State.TITLE && newState == State.MULTI_INPUT_NAME) {
            clientName = System.getProperty("user.name");
        } else if (nowState == State.TITLE && newState == State.RULE) {
            printRualShift = 0;
        } else if (nowState == State.TITLE && newState == State.ABOUT) {
            //
        } else if (nowState == State.LOCAL_PLAYING && newState == State.TITLE) {
            //
        } else if (nowState == State.LOCAL_PLAYING && newState == State.LOCAL_FINISH) {
            userTime = (System.currentTimeMillis() - startTime);
        } else if (nowState == State.LOCAL_FINISH && newState == State.TITLE) {
            //
        } else if (nowState == State.MULTI_INPUT_NAME && newState == State.TITLE) {
            //
        } else if (nowState == State.MULTI_INPUT_NAME && newState == State.MULTI_MODE_CHOOSE) {
            //
        } else if (nowState == State.MULTI_MODE_CHOOSE && newState == State.TITLE) {
            //
        } else if (nowState == State.MULTI_MODE_CHOOSE && newState == State.MULTI_WAIT_START) {
            playersImageIndex = new int[]{-1, -1, -1, -1, -1};
            playersName = new String[5];
            server = new Server(this);
            client = new Client(this, server.getGameGroupAddress());
        } else if (nowState == State.MULTI_MODE_CHOOSE && newState == State.MULTI_LIST_SERVER) {
            playersImageIndex = new int[]{-1, -1, -1, -1, -1};
            playersName = new String[5];
            client = new Client(this);
            serverList = client.searchGame();
            serverListSel = serverList.size() - 1;
        } else if (nowState == State.MULTI_LIST_SERVER && newState == State.MULTI_WAIT_START) {
            playersImageIndex[client.getId()] = 0;
        } else if (nowState == State.MULTI_LIST_SERVER && newState == State.MULTI_JOIN_FAIL) {
            //
        } else if (nowState == State.MULTI_LIST_SERVER && newState == State.MULTI_MODE_CHOOSE) {
            //
        } else if (nowState == State.MULTI_JOIN_FAIL && newState == State.MULTI_LIST_SERVER) {
            playersImageIndex = new int[]{-1, -1, -1, -1, -1};
            playersName = new String[5];
            serverList = client.searchGame();
            serverListSel = serverList.size() - 1;
        } else if (nowState == State.MULTI_WAIT_START && newState == State.MULTI_MODE_CHOOSE) {
            //
        } else if (nowState == State.MULTI_WAIT_START && newState == State.MULTI_PLAYING) {
            //*******
        } else if (nowState == State.MULTI_PLAYING && newState == State.TITLE) {
            //
        } else if (nowState == State.MULTI_PLAYING && newState == State.MULTI_FINISH) {
            //
        } else if (nowState == State.MULTI_FINISH && newState == State.TITLE) {
            //
        } else if (nowState == State.RULE && newState == State.TITLE) {
            //
        } else if (nowState == State.ABOUT && newState == State.TITLE) {
            //
        } else {
            throw new RuntimeException("預測外的狀態改變(前置處理):" + nowState.name() + " -> " + newState.name());
        }
    }

    /** 被狀態管理器所呼叫, 做一些狀態改變的後置處理(狀態到退) */
    private void stateChangeHandling(State oldState, State nowState) {
        if (oldState == nowState) {
            //
        } else if (oldState == State.TITLE && nowState == State.LOCAL_PLAYING) {
            startTime = System.currentTimeMillis();
        } else if (oldState == State.TITLE && nowState == State.MULTI_INPUT_NAME) {
            //
        } else if (oldState == State.TITLE && nowState == State.RULE) {
            //
        } else if (oldState == State.TITLE && nowState == State.ABOUT) {
            //
        } else if (oldState == State.LOCAL_PLAYING && nowState == State.TITLE) {
            maze = null;
            p0 = null;
            p1 = null;
        } else if (oldState == State.LOCAL_PLAYING && nowState == State.LOCAL_FINISH) {
            //
        } else if (oldState == State.LOCAL_FINISH && nowState == State.TITLE) {
            maze = null;
            p0 = null;
            p1 = null;
        } else if (oldState == State.MULTI_INPUT_NAME && nowState == State.TITLE) {
            //
        } else if (oldState == State.MULTI_INPUT_NAME && nowState == State.MULTI_MODE_CHOOSE) {
            //
        } else if (oldState == State.MULTI_MODE_CHOOSE && nowState == State.TITLE) {
            //
        } else if (oldState == State.MULTI_MODE_CHOOSE && nowState == State.MULTI_WAIT_START) {
            //
        } else if (oldState == State.MULTI_MODE_CHOOSE && nowState == State.MULTI_LIST_SERVER) {
            //
        } else if (oldState == State.MULTI_LIST_SERVER && nowState == State.MULTI_MODE_CHOOSE) {
            client.close();
            client = null;
            serverList = null;
            serverListSel = -1;
            drawArray = null;
            playersImageIndex = null;
            playersName = null;
        } else if (oldState == State.MULTI_LIST_SERVER && nowState == State.MULTI_JOIN_FAIL) {
            //
        } else if (oldState == State.MULTI_LIST_SERVER && nowState == State.MULTI_WAIT_START) {
            //
        } else if (oldState == State.MULTI_JOIN_FAIL && nowState == State.MULTI_LIST_SERVER) {
            serverList = null;
            serverListSel = -1;
            drawArray = null;
            playersImageIndex = null;
            playersName = null;
        } else if (oldState == State.MULTI_WAIT_START && nowState == State.MULTI_MODE_CHOOSE) {
            client.close();
            client = null;
            if (multiModeMenu == MultiModeMenu.SERVER) {
                server.close();
                server = null;
            }
            serverList = null;
            serverListSel = -1;
            drawArray = null;
            playersImageIndex = null;
            playersName = null;
        } else if (oldState == State.MULTI_WAIT_START && nowState == State.MULTI_PLAYING) {
            //
        } else if (oldState == State.MULTI_PLAYING && nowState == State.TITLE) {
            client.close();
            client = null;
            if (multiModeMenu == MultiModeMenu.SERVER) {
                server.close();
                server = null;
            }
            serverList = null;
            serverListSel = -1;
            drawArray = null;
            playersImageIndex = null;
            playersName = null;
        } else if (oldState == State.MULTI_PLAYING && nowState == State.MULTI_FINISH) {
            client.close();
            client = null;
            if (multiModeMenu == MultiModeMenu.SERVER) {
                server.close();
                server = null;
            }
            serverList = null;
            serverListSel = -1;
            drawArray = null;
            playersName = null;
        } else if (oldState == State.MULTI_FINISH && nowState == State.TITLE) {
            playersImageIndex = null;
        } else if (oldState == State.RULE && nowState == State.TITLE) {
            //
        } else if (oldState == State.ABOUT && nowState == State.TITLE) {
            //
        } else {
            throw new RuntimeException("預測外的狀態改變(後續處理):" + oldState.name() + " -> " + nowState.name());
        }
    }

    void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        switch (sm.state) {
            case TITLE:
                kp_TITLE(keyCode);
                break;
            case LOCAL_FINISH:
                kp_LOCAL_FINISH(keyCode);
                break;
            case MULTI_INPUT_NAME:
                kp_MULTI_INPUT_NAME(keyCode, e.getKeyChar());
                break;
            case MULTI_MODE_CHOOSE:
                kp_MULTI_SELECT_MODE(keyCode);
                break;
            case MULTI_LIST_SERVER:
                kp_MULTI_LIST_SERVER(keyCode);
                break;
            case MULTI_JOIN_FAIL:
                kp_MULTI_JOIN_FAIL(keyCode);
                break;
            case MULTI_WAIT_START:
                kp_MULTI_WAIT_START(keyCode);
                break;
            case MULTI_FINISH:
                kp_MULTI_FINISH(keyCode);
                break;
            case RULE:
                kp_RULE(keyCode);
                break;
            case ABOUT:
                kp_ABOUT(keyCode);
                break;
        }
    }

    private void kp_TITLE(int keyCode) {
        if (keyCode == KeyEvent.VK_UP || keyCode == KeyEvent.VK_W) {                     //如果按鍵碼為[↑]則改變選單項目
            if (titleMenu.ordinal() > 0) {
                titleMenu = TitleMenu.values()[titleMenu.ordinal() - 1];
            }
        } else if (keyCode == KeyEvent.VK_DOWN || keyCode == KeyEvent.VK_S) {            //如果按鍵碼為[↓]則改變選單項目
            if (titleMenu.ordinal() < TitleMenu.values().length - 1) {
                titleMenu = TitleMenu.values()[titleMenu.ordinal() + 1];
            }
        } else if (keyCode == KeyEvent.VK_ESCAPE) {
            if (titleMenu == titleMenu.EXIT) {
                System.exit(0);
            } else {
                titleMenu = titleMenu.EXIT;
            }
        } else if (keyCode == KeyEvent.VK_ENTER || keyCode == KeyEvent.VK_SPACE) {
            switch (titleMenu) {
                case LOCALGAME:
                    sm.changeState(State.LOCAL_PLAYING);
                    break;
                case MULTIGAME:
                    sm.changeState(State.MULTI_INPUT_NAME);
                    break;
                case RULE:
                    sm.changeState(State.RULE);
                    break;
                case ABOUT:
                    sm.changeState(State.ABOUT);
                    break;
                case EXIT:
                    System.exit(0);
            }
        }
    }

    private void kp_LOCAL_FINISH(int keyCode) {
        if (keyCode == KeyEvent.VK_ENTER || keyCode == KeyEvent.VK_ESCAPE || keyCode == KeyEvent.VK_SPACE) {
            sm.changeState(State.TITLE);
        }
    }

    private void kp_MULTI_INPUT_NAME(int keyCode, char keyChar) {
        if (keyChar >= 32 && keyChar <= 126 && keyChar != Packet.SS.charAt(0) && clientName.length() < ClientNameLimit) {
            clientName += keyChar;
        } else if (keyCode == KeyEvent.VK_BACK_SPACE && clientName.length() > 0) {
            clientName = clientName.substring(0, clientName.length() - 1);
        } else if (keyCode == KeyEvent.VK_ESCAPE) {
            sm.changeState(State.TITLE);
        } else if (keyCode == KeyEvent.VK_ENTER) {
            sm.changeState(State.MULTI_MODE_CHOOSE);
        }
    }

    private void kp_MULTI_SELECT_MODE(int keyCode) {
        if (keyCode == KeyEvent.VK_UP) {                     //如果按鍵碼為[↑]則改變選單項目
            if (multiModeMenu.ordinal() > 0) {
                multiModeMenu = MultiModeMenu.values()[multiModeMenu.ordinal() - 1];
            }
        } else if (keyCode == KeyEvent.VK_DOWN) {            //如果按鍵碼為[↓]則改變選單項目
            if (multiModeMenu.ordinal() < MultiModeMenu.values().length - 1) {
                multiModeMenu = MultiModeMenu.values()[multiModeMenu.ordinal() + 1];
            }
        } else if (keyCode == KeyEvent.VK_ESCAPE) {
            if (multiModeMenu.ordinal() == MultiModeMenu.values().length - 1) {
                sm.changeState(State.TITLE);
            } else {
                multiModeMenu = MultiModeMenu.BREAK;
            }
        } else if (keyCode == KeyEvent.VK_ENTER || keyCode == KeyEvent.VK_SPACE) {
            if (multiModeMenu == MultiModeMenu.SERVER) {
                sm.changeState(State.MULTI_WAIT_START);
            } else if (multiModeMenu == MultiModeMenu.CLIENT) {
                sm.changeState(State.MULTI_LIST_SERVER);
            } else if (multiModeMenu == MultiModeMenu.BREAK) {
                sm.changeState(State.TITLE);
            }
        }
    }

    private void kp_MULTI_LIST_SERVER(int keyCode) {
        if (keyCode == KeyEvent.VK_UP) {                     //如果按鍵碼為[↑]則改變選單項目
            if (serverListSel > 0) {
                serverListSel--;
            }
        } else if (keyCode == KeyEvent.VK_DOWN) {            //如果按鍵碼為[↓]則改變選單項目
            if (serverListSel < serverList.size() - 1) {
                serverListSel++;
            }
        } else if (keyCode == KeyEvent.VK_ESCAPE) {
            sm.changeState(State.MULTI_MODE_CHOOSE);
        } else if (keyCode == KeyEvent.VK_ENTER) {
            if (serverListSel > -1 && serverListSel < serverList.size()) {
                String addr = serverList.get(serverListSel).split(Packet.SS)[0];
                if (client.joinGame(addr)) {
                    sm.changeState(State.MULTI_WAIT_START);
                } else {
                    sm.changeState(State.MULTI_JOIN_FAIL);
                }
            }
        }
    }

    private void kp_MULTI_JOIN_FAIL(int keyCode) {
        if (keyCode == KeyEvent.VK_ESCAPE) {
            sm.changeState(State.MULTI_LIST_SERVER);
        }
    }

    private void kp_MULTI_WAIT_START(int keyCode) {
        int newindex = playersImageIndex[client.getId()];
        if (keyCode == KeyEvent.VK_LEFT || keyCode == KeyEvent.VK_RIGHT) {
            keyState = keyCode;
        }
        if (keyCode == KeyEvent.VK_LEFT) {
            newindex = (newindex == 0) ? 5 : newindex - 1;
            client.sendToGameGroup(Packet.SYNC_PLAYER_INFO.setData(client.getId() + Packet.SS + clientName + Packet.SS + newindex), Server.GAME_PORT);
            client.sendToGameGroup(Packet.SYNC_PLAYER_INFO.setData(client.getId() + Packet.SS + clientName + Packet.SS + newindex), Client.GAME_PORT);
        } else if (keyCode == KeyEvent.VK_RIGHT) {
            newindex = (newindex == 5) ? 0 : newindex + 1;
            client.sendToGameGroup(Packet.SYNC_PLAYER_INFO.setData(client.getId() + Packet.SS + clientName + Packet.SS + newindex), Server.GAME_PORT);
            client.sendToGameGroup(Packet.SYNC_PLAYER_INFO.setData(client.getId() + Packet.SS + clientName + Packet.SS + newindex), Client.GAME_PORT);
        } else if (keyCode == KeyEvent.VK_ESCAPE) {
            breakMULTI_MODE_SELECT();
        } else if (keyCode == KeyEvent.VK_ENTER) {
            if (multiModeMenu == MultiModeMenu.SERVER) {
                if (server.startGame()) {
                    //sm.changeState(State.MULTI_PLAYING);
                }
            }
        }
    }

    private void kp_MULTI_FINISH(int keyCode) {
        if (keyCode == KeyEvent.VK_ESCAPE) {
            sm.changeState(State.TITLE);
        }
    }

    private void kp_RULE(int keyCode) {
        if (keyCode == KeyEvent.VK_UP && printRualShift > 0) {
            printRualShift -= 1;
        } else if (keyCode == KeyEvent.VK_DOWN && printRualShift < RULE_MESSAGE.length - 1) {
            printRualShift += 1;
        } else if (keyCode == KeyEvent.VK_ESCAPE) {
            sm.changeState(State.TITLE);
        }
    }

    private void kp_ABOUT(int keyCode) {
        if (keyCode == KeyEvent.VK_ESCAPE) {
            sm.changeState(State.TITLE);
        }
    }

    void keyReleased(KeyEvent e) {
        if (sm.state() == State.MULTI_WAIT_START) {
            kr_MULTI_WAIT_START(e.getKeyCode());
        }
    }

    private void kr_MULTI_WAIT_START(int keyCode) {
        if (keyCode == KeyEvent.VK_LEFT || keyCode == KeyEvent.VK_RIGHT) {
            keyState = 0;
        }
    }

    /**
     * 按鍵處理
     */
    private synchronized void checkKey() {
        keyListener.poll();
        switch (sm.state()) {
            case LOCAL_PLAYING:
                ck_LOCAL_PLAYING();
                break;
            case MULTI_PLAYING:
                ck_MULTI_PLAYING();
                break;
        }
    }

    private void ck_LOCAL_PLAYING() {
        if (p0 == null || p1 == null) {
            return;
        }
        ck_PlayerMove(p0);
        ck_PlayerMove(p1);
        if (keyListener.isDown(KeyEvent.VK_CONTROL) && keyListener.isDownOnce(KeyEvent.VK_F6)) {
            this.debug_MaskFlag = !this.debug_MaskFlag;
        }//*/

        if (maze.chechExit(p0) || maze.chechExit(p1)) {
            sm.changeState(State.LOCAL_FINISH.setMsg((maze.chechExit(p0)) ? "1P" : "2P"));
        }//*/

        if (keyListener.isDownOnce(KeyEvent.VK_ESCAPE)) {
            sm.changeState(State.TITLE);
        }
    }

    private void ck_PlayerMove(Player p) {
        int mxp = 0, myp = 0;
        if (client != null || p.id == 0) {
            if (keyListener.isDown(KeyEvent.VK_UP)) {
                myp = -1;
            } else if (keyListener.isDown(KeyEvent.VK_DOWN)) {
                myp = 1;
            }
            if (keyListener.isDown(KeyEvent.VK_LEFT)) {
                mxp = -1;
            } else if (keyListener.isDown(KeyEvent.VK_RIGHT)) {
                mxp = 1;
            }
        } else {
            if (keyListener.isDown(KeyEvent.VK_W)) {
                myp = -1;
            } else if (keyListener.isDown(KeyEvent.VK_S)) {
                myp = 1;
            }
            if (keyListener.isDown(KeyEvent.VK_A)) {
                mxp = -1;
            } else if (keyListener.isDown(KeyEvent.VK_D)) {
                mxp = 1;
            }
        }

        if (mxp != 0 || myp != 0) {
            if (sm.state == State.LOCAL_PLAYING) {
                maze.movePlayer(mxp, myp, p.id);
            } else {
                client.sendToGameGroup(Packet.PLAYER_MOVE_ASK.setData(client.getId() + Packet.SS + mxp + Packet.SS + myp), Server.GAME_PORT);
            }
        }
    }

    /**
     * 多人遊戲中的按鍵處理
     */
    private void ck_MULTI_PLAYING() {
        ck_PlayerMove(p0);
        if (keyListener.isDownOnce(KeyEvent.VK_ESCAPE)) {          //如果按鍵為[ESC]則離開遊戲
            client.sendToGameGroup(Packet.EXIT_GAME.setData(client.getId() + ""), Server.GAME_PORT);
            sm.changeState(State.TITLE);
        }

        if (keyListener.isDown(KeyEvent.VK_CONTROL) && keyListener.isDown(KeyEvent.VK_SHIFT) && keyListener.isDownOnce(KeyEvent.VK_F6)) {
            this.debug_MaskFlag = !this.debug_MaskFlag;
        }//*/
    }

    /**
     * 只指定的比例位置與字型印出文字
     * @param g 繪圖的目標
     * @param str 文字串
     * @param hPer 垂直位置比例
     * @param vPer 水平位置比例
     * @param font 字形
     */
    private void drawString(Graphics g, String str, double hPer, double vPer, Font font) {
        g.setColor(Color.BLACK);
        if (font != null) {
            g.setFont(font);
        }
        int hPos = calcHorizontalSitePer(g, str, hPer);
        int vPos = calcVerticalSitePer(g, vPer);
        g.drawString(str, hPos, vPos);
    }

    private void drawString(Graphics g, String str, double hPer, double vPer) {
        drawString(g, str, hPer, vPer, defFont);
    }

    /**
     * 畫畫面標題
     * @param g 繪圖的目標
     * @param title 標題字串
     */
    private void drawFrameTitle(Graphics g, String title) {
        drawString(g, title, 0.5, 0.25, defFont.deriveFont(Font.BOLD, defFont.getSize2D() * 3));
    }

    /**
     * 畫按鍵提示
     * @param g 繪圖的目標
     * @param tip 提示字串
     */
    private void drawKeyTip(Graphics g, String tip) {
        drawString(g, tip, 0.5, 1, defFont);
    }

    /**
     * 自訂的繪圖模式
     */
    private synchronized void drawFrame() {
        Graphics2D backG;

        //取得背景畫布
        backG = backstage.createGraphics();

        if (sm.state() == State.LOCAL_PLAYING || sm.state() == State.MULTI_PLAYING) {
            backG.setColor(Color.BLACK);
        } else {
            //設定文字反鋸齒
            backG.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            backG.setColor(Color.WHITE);
        }
        backG.fillRect(0, 0, getW(), getH());

        switch (sm.state()) {
            case TITLE:
                df_TITLE(backG);
                break;
            case LOCAL_PLAYING:
                df_LOCAL_PLAYING(backG);
                break;
            case LOCAL_FINISH:
                df_LOCAL_FINISH(backG);
                break;
            case MULTI_INPUT_NAME:
                df_MULTI_INPUT_NAME(backG);
                break;
            case MULTI_MODE_CHOOSE:
                df_MULTI_SELECT_MODE(backG);
                break;
            case MULTI_LIST_SERVER:
                df_MULTI_LIST_SERVER(backG);
                break;
            case MULTI_JOIN_FAIL:
                df_MULTI_JOIN_FAIL(backG);
                break;
            case MULTI_WAIT_START:
                df_MULTI_WAIT_START(backG);
                break;
            case MULTI_PLAYING:
                df_MULTI_PLAYING(backG);
                break;
            case MULTI_FINISH:
                df_MULTI_FINISH(backG);
                break;
            case RULE:
                df_RULE(backG);
                break;
            case ABOUT:
                df_ABOUT(backG);
                break;
        }

        Graphics frontG = null;
        try {
            frontG = bufferStrategy.getDrawGraphics(); //取得前景畫布

            //如果不是全螢幕模式(視窗模式)就位移繪圖原點掠過視窗邊框
            if (!DisplayManager.isFullScreenMode()) {
                frontG.translate(getInsets().left, getInsets().top);
            }//非視窗模式時候其實不需要*/

            //將背景畫到前景畫布
            frontG.drawImage(backstage, 0, 0, null);

            bufferStrategy.show(); //teacher say:"啪!!"的一聲 將前景顯示在畫面上XD

            getToolkit().sync(); //同步繪圖(Linux下不同步的問題處理)
        } finally {
            if (backG != null) {
                backG.dispose(); //釋放背景畫布
            }
            if (frontG != null) {
                frontG.dispose(); //釋放前景畫布
            }
        }
    }

    /**
     * 畫遊戲標題畫面
     * @param g 繪圖的目標
     */
    private void df_TITLE(Graphics g) {
        //繪出標題 X:字串置中 Y:畫面高/3-字形高/2
        drawFrameTitle(g, MainFrame.GAME_TITLE);

        titleMenu.paint(g, getW(), getH(), defFont);

        //按鍵提示 X:字串置中 Y:畫面底部
        drawKeyTip(g, "[ENTER]or[SPACE]:選擇項目 [ESC]:離開");
    }
    Shape[] localGameClip = new Shape[2];

    /**
     * 畫多人連線遊戲
     * @param g 繪圖的目標
     */
    private void df_LOCAL_PLAYING(Graphics g) {
        if (p0 == null || p1 == null) {
            return;
        }
        localGameClip[0] = p0.getLookSpace();
        localGameClip[1] = p1.getLookSpace();
        maze.paint(g, getW(), getH(), (debug_MaskFlag ? null : localGameClip), null);
    }

    /**
     * 畫單人模式完成遊戲的畫面
     * @param g 繪圖的目標
     */
    private void df_LOCAL_FINISH(Graphics g) {
        String ds = "";
        int vPos, hPos;

        g.setColor(Color.BLACK);
        g.setFont(defFont);

        hPos = calcHorizontalSitePer(g, ds, 0.5) - p0.getImage().getWidth(null) / 2;
        vPos = calcVerticalSitePer(g, 0.4);
        if (sm.state().msg.equals("1P")) {
            ds = "Player 1 走出迷宮!";
            g.drawImage(p0.getImage(), hPos, vPos, null);
        } else if (sm.state().msg.equals("2P")) {
            ds = "Player 2 走出迷宮!";
            g.drawImage(p1.getImage(), hPos, vPos, null);
        }
        drawString(g, ds, 0.5, 0.25);

        ds = "一共用了:" + ((float) userTime / 1000.0) + "秒";
        hPos = calcHorizontalSitePer(g, ds, 0.5);
        vPos = calcVerticalSitePer(g, 0.55);
        g.drawString(ds, hPos, vPos);

        drawKeyTip(g, "[ESC]/[ENTER]/[SPACE]:離開");
    }//*/

    private void df_MULTI_INPUT_NAME(Graphics g) {
        drawFrameTitle(g, "輸入玩家名稱");

        drawString(g, "這個名稱用來辨識玩家以及顯示主導遊戲用", 0.5, 0.45);
        drawString(g, "限制:英數符號半形20字, '" + Packet.SS + "' 字元無法使用", 0.5, 0.5);
        g.setFont(Font.decode(Font.MONOSPACED).deriveFont((float) defFont.getSize()));
        String str = "                    ";
        int rectWeight = g.getFontMetrics().stringWidth(str) + 8, fh = g.getFontMetrics().getHeight();
        int vpos = calcVerticalSitePer(g, 0.7);
        int hpos = (getW() - rectWeight) / 2;
        g.draw3DRect(hpos, vpos, rectWeight, fh, running);
        g.drawString(clientName, hpos + 4, vpos + fh - 4);
        int sw = g.getFontMetrics().stringWidth(clientName);
        g.drawLine(sw + hpos + 4, vpos + 4, sw + hpos + 4, vpos + fh - 2);

        drawKeyTip(g, "[ENTER]:確定 [ESC]:回主選單");
    }

    private void df_MULTI_SELECT_MODE(Graphics g) {
        drawFrameTitle(g, "多人遊戲模式選擇");

        multiModeMenu.paint(g, getW(), getH(), defFont);

        drawKeyTip(g, "[ENTER]or[SPACE]:選擇項目 [ESC]:回主選單");
    }

    private void df_MULTI_LIST_SERVER(Graphics g) {
        drawFrameTitle(g, "選擇要加入的遊戲");

        String ds;
        g.setFont(defFont);
        int vpos = this.calcVerticalSitePer(g, 0.45);
        if (serverList != null && serverList.size() > 0) {
            int hpos = this.calcHorizontalSitePer(g, serverList.get(0), 0.5);
            int gNum;
            String gName;
            for (int i = 0; i < serverList.size(); i++) {
                String addr = serverList.get(i).split(Packet.SS)[0];
                gNum = Integer.valueOf(addr.substring(addr.lastIndexOf(".") + 1));
                gName = serverList.get(i).split(Packet.SS)[1];
                ds = gNum + ":" + gName;
                if (serverListSel == i) {
                    ds = "[" + ds + "]";
                }
                g.drawString(ds, hpos, vpos += g.getFontMetrics().getHeight() + 4);
            }
        } else {
            ds = "<沒有可以加入的遊戲>";
            g.drawString(ds, calcHorizontalSitePer(g, ds, 0.5), vpos);
        }

        ds = "[ESC]:回模式選擇";
        if (serverList.size() > 0) {
            ds = "[ENTER]:選擇要加入的遊戲 " + ds;
        }
        drawKeyTip(g, ds);
    }

    private void df_MULTI_JOIN_FAIL(Graphics g) {
        drawFrameTitle(g, "加入遊戲失敗");

        String ds = "您所選擇的伺服器可能已經滿人或是關閉";
        drawString(g, ds, calcHorizontalSitePer(g, ds, 0.5), this.calcVerticalSitePer(g, 0.5));

        drawKeyTip(g, "[ESC]:回遊戲選擇畫面");
    }
    private Point baseof_dfMWS;
    private GradientPaint gp1of_dfMWS;
    private GradientPaint gp2of_dfMWS;

    private void df_MULTI_WAIT_START(Graphics2D g) {
        g.drawImage(ImageKit.PSBI, 0, 0, null);

        drawString(g, client.getGameName(), 0.5, 0.15, defFont.deriveFont(Font.BOLD, getH() / 16));

        int now = playersImageIndex[client.getId()], prec = (now == 0) ? 5 : now - 1, next = (now == 5) ? 0 : now + 1;
        Image playerImage, pi, ni;
        try {
            playerImage = ImageKit.getPlayerImageMapL3(now)[0][0];
            if (baseof_dfMWS == null) {
                baseof_dfMWS = new Point((getW() - playerImage.getWidth(null)) / 2, (getH() - playerImage.getHeight(null)) / 2);
            }
            g.setFont(defFont);
            g.drawString(clientName, this.calcHorizontalSitePer(g, clientName, 0.5), (getH() - playerImage.getHeight(null)) / 2 - g.getFontMetrics().getHeight());
            g.drawImage(playerImage, baseof_dfMWS.x, baseof_dfMWS.y, null);

            try {
                pi = ImageKit.getPlayerImageMapL2(prec)[0][0];
                g.drawImage(pi, baseof_dfMWS.x - pi.getWidth(null), baseof_dfMWS.y, null);
                if (gp1of_dfMWS == null) {
                    gp1of_dfMWS = new GradientPaint(baseof_dfMWS.x - pi.getWidth(null), baseof_dfMWS.y, bc,
                            baseof_dfMWS.x - pi.getWidth(null) / 2, baseof_dfMWS.y, fc, true);
                }
                g.setPaint(gp1of_dfMWS);//*/
                g.fillRect(baseof_dfMWS.x - pi.getWidth(null), baseof_dfMWS.y, pi.getWidth(null), pi.getHeight(null));
                g.setPaint(null);
            } catch (NullPointerException ex) {
            }
            try {
                ni = ImageKit.getPlayerImageMapL2(next)[0][0];
                g.drawImage(ni, baseof_dfMWS.x + playerImage.getWidth(null), baseof_dfMWS.y, null);
                if (gp2of_dfMWS == null) {
                    gp2of_dfMWS = new GradientPaint(baseof_dfMWS.x + playerImage.getWidth(null), baseof_dfMWS.y, bc,
                            baseof_dfMWS.x + playerImage.getWidth(null) + ni.getWidth(null) / 2, baseof_dfMWS.y, fc, true);
                }
                g.setPaint(gp2of_dfMWS);//*/
                g.fillRect(baseof_dfMWS.x + playerImage.getWidth(null), baseof_dfMWS.y, ni.getWidth(null), ni.getHeight(null));
                g.setPaint(null);
            } catch (NullPointerException ex) {
            }
        } catch (NullPointerException ex) {
        }

        g.setColor(Color.DARK_GRAY);
        int lux = getW() / 16, luy = getH() / 5, rux = getW() * 15 / 16 - getH() / 7, ldy = luy + getH() * 11 / 26;
        String ws = "<Wait Joining>";
        g.setFont(defFont);

        try {
            playerImage = ImageKit.getPlayerImageMapL2(playersImageIndex[drawArray[0]])[0][0];
            g.drawString(playersName[drawArray[0]], lux, luy - g.getFontMetrics().getHeight());
            g.drawImage(playerImage, lux, luy, null);
        } catch (NullPointerException ex) {
            g.drawString(ws, lux, luy);
        }

        try {
            playerImage = ImageKit.getPlayerImageMapL2(playersImageIndex[drawArray[1]])[0][0];
            g.drawString(playersName[drawArray[1]], rux, luy - g.getFontMetrics().getHeight());
            g.drawImage(playerImage, rux, luy, null);
        } catch (NullPointerException ex) {
            g.drawString(ws, rux, luy);
        }

        try {
            playerImage = ImageKit.getPlayerImageMapL2(playersImageIndex[drawArray[2]])[0][0];
            g.drawString(playersName[drawArray[2]], lux, ldy - g.getFontMetrics().getHeight());
            g.drawImage(playerImage, lux, ldy, null);
        } catch (NullPointerException ex) {
            g.drawString(ws, lux, ldy);
        }

        try {
            playerImage = ImageKit.getPlayerImageMapL2(playersImageIndex[drawArray[3]])[0][0];
            g.drawString(playersName[drawArray[3]], rux, ldy - g.getFontMetrics().getHeight());
            g.drawImage(playerImage, rux, ldy, null);
        } catch (NullPointerException ex) {
            g.drawString(ws, rux, ldy);
        }

        Color lc = Color.GRAY, rc = Color.GRAY;
        int w = getW() / 25, h = getH() / 11;
        int x = getW() / 5 + w + w / 2, y = (getH() - h) / 2;
        int[] xpoint = {x, x - w, x}, xpoint0 = new int[3], ypoint = {y, y + h / 2, y + h};

        if (keyState == KeyEvent.VK_LEFT) {
            lc = Color.BLACK;
        } else if (keyState == KeyEvent.VK_RIGHT) {
            rc = Color.BLACK;
        }//*/

        g.setColor(lc);
        for (int i = 0; i < 5; i++) {
            xpoint0[0] = (xpoint[0] += 1) + w / 4;
            xpoint0[1] = (xpoint[1] += 1) + w / 4;
            xpoint0[2] = (xpoint[2] += 1) + w / 4;
            g.drawPolyline(xpoint, ypoint, 3);
            g.drawPolyline(xpoint0, ypoint, 3);
        }

        String kt = "[ESC]:回多人遊戲模式選擇";
        if (multiModeMenu == MultiModeMenu.SERVER) {
            kt = "[Enter]:開始遊戲 " + kt;
        }
        drawKeyTip(g, kt);

        ((Graphics2D) g).rotate(Math.toRadians(180), getW() / 2, getH() / 2);
        g.setColor(rc);
        for (int i = 0; i < 5; i++) {
            xpoint0[0] = (xpoint[0] += 1) + w / 4;
            xpoint0[1] = (xpoint[1] += 1) + w / 4;
            xpoint0[2] = (xpoint[2] += 1) + w / 4;
            g.drawPolyline(xpoint, ypoint, 3);
            g.drawPolyline(xpoint0, ypoint, 3);
        }
        xpoint = null;
        xpoint0 = null;
        ypoint = null;
    }
    Shape[] multiGameClip = new Shape[1];

    private void df_MULTI_PLAYING(Graphics g) {
        multiGameClip[0] = client.getMaze().getPlayer(client.getId()).getLookSpace();
        client.getMaze().paint(g, getW(), getH(), (debug_MaskFlag ? null : multiGameClip), playersName);
    }

    /**
     * 畫出多人遊戲的出關畫面
     * @param g 繪圖的目標
     */
    private void df_MULTI_FINISH(Graphics g) {
        drawFrameTitle(g, "玩家" + sm.state().msg + " 走出迷宮!");

        int mid = Integer.valueOf(sm.state().msg.split(Packet.SS)[0]);
        Image img = ImageKit.getPlayerImageMapL2(mid)[0][0];
        int hPos = (int) (getW() * 0.4);
        int vPos = calcVerticalSitePer(g, 0.4);
        g.drawImage(img, hPos, vPos, null);

        drawKeyTip(g, "[ESC]:回標題畫面");
    }

    private void df_RULE(Graphics g) {
        drawFrameTitle(g, "遊戲規則");

        String ds;
        int vPos, hPos;

        g.setColor(Color.BLUE);
        g.draw3DRect((int) (getW() * 0.18),
                (int) (getH() * 0.25),
                (int) (getW() - getW() * 0.18 * 2),
                (int) (getH() * 0.66), true);
        g.setFont(defFont);
        hPos = calcHorizontalSitePer(g, MainFrame.RULE_MESSAGE[0], 0.26);
        vPos = calcVerticalSitePer(g, 0.32);
        for (int i = printRualShift; i < MainFrame.RULE_MESSAGE.length; i++) {
            ds = MainFrame.RULE_MESSAGE[i];
            if (vPos > getH() * 9 / 10) {
                break;
            }
            g.drawString(ds, hPos, vPos);
            vPos += (g.getFontMetrics().getHeight() + 2);
        }//*/

        ds = "[ESC]:回標題畫面 ";
        if (printRualShift == 0) {
            ds += "[↓]:移動";
        } else if (printRualShift > 0 && printRualShift < RULE_MESSAGE.length - 1) {
            ds += "[↑][↓]:移動";
        } else {
            ds += "[↑]:移動";
        }
        drawKeyTip(g, ds);
    }

    /**
     * 畫關於本程式畫面
     * @param g 繪圖的目標
     */
    private void df_ABOUT(Graphics g) {
        drawFrameTitle(g, "關於本程式");

        String ds;
        int vPos, hPos;

        g.setColor(Color.BLUE);
        g.setFont(defFont.deriveFont((float) 16.0));

        hPos = calcHorizontalSitePer(g, MainFrame.ABOUT_MESSAGE[1], 0.24);
        vPos = calcVerticalSitePer(g, 0.45);
        for (int i = 0; i < MainFrame.ABOUT_MESSAGE.length; i++) {
            ds = MainFrame.ABOUT_MESSAGE[i];
            g.drawString(ds, hPos, vPos);
            vPos += (g.getFontMetrics().getHeight() + 4);
        }

        hPos = (int) (getW() * 0.6);
        vPos = calcVerticalSitePer(g, 0.44);
        g.drawImage(ImageKit.AboutImage.getScaledInstance(getW() / 4, getW() / 4, Image.SCALE_SMOOTH), hPos, vPos, null);

        drawKeyTip(g, "[ESC]:回標題畫面");
    }

    public Maze getMaze() {
        return maze;
    }

    /**
     * 取得畫面垂直比例的位置
     * @param g 繪圖的目標
     * @param hPrt 相對於繪圖目標高度的比例
     */
    private int calcVerticalSitePer(Graphics g, double hPrt) {
        return calcVerticalSitePer(getContentPane().getHeight(), g, hPrt);
    }

    /**
     * 取得畫面垂直比例的位置
     * @param h 畫面高
     * @param g 繪圖的目標
     * @param hPrt 相對於繪圖目標高度的比例
     */
    public static int calcVerticalSitePer(int h, Graphics g, double hPrt) {
        return (int) (h * hPrt) - (g.getFontMetrics().getHeight() / 2);
    }

    /**
     * 取得畫面水平比例的位置
     * @param g 繪圖的目標
     * @param s 要量出寬度的字串
     * @param vPrt 相對於繪圖目標寬度的比例
     */
    private int calcHorizontalSitePer(Graphics g, String s, double vPrt) {
        return calcHorizontalSitePer(getW(), g, s, vPrt);
    }

    /**
     * 取得畫面水平比例的位置
     * @param w 畫面寬
     * @param g 繪圖的目標
     * @param s 要量出寬度的字串
     * @param vPrt 相對於繪圖目標寬度的比例
     */
    public static int calcHorizontalSitePer(int w, Graphics g, String s, double vPrt) {
        return (int) ((w - g.getFontMetrics().stringWidth(s)) * vPrt);
    }

    public int getH() {
        return super.getContentPane().getHeight();
    }

    public int getW() {
        return super.getContentPane().getWidth();
    }

    /**
     * stop main thread
     */
    public void stop() {
        running = false;
        mainThread.interrupt();
    }

    /**
     * <pre>
     * main thread<br/>
     * Loop 下列三項工作<br/>
     * 1.check keys state do ...(內含poll keys state)<br/>
     * 2.draw frame<br/>
     * 3.sleep<br/>
     * </pre>
     */
    public void run() {
        running = true;
        while (running) {
            checkKey();
            drawFrame();
            sleep(FREQUENCY);
        }
    }

    /**
     * sleep 方法捷徑(解省try catch次數)
     * @param millis 毫秒
     */
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
        }
    }

    /**
     * 程式進入點
     * @param args 命令列參數
     */
    public static void main(final String[] args) {
        /* 因為javax.swing下的所有類別有原生的執行緒安全問題
         * 藉由EventQueue類別將視窗執行緒排入主執行序中再執行
         * 以避免執行緒安全性的問題 (by Java 6 API Document ) */
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                int s = JOptionPane.showConfirmDialog(null, "full screen?", MainFrame.GAME_TITLE, JOptionPane.YES_NO_OPTION);
                boolean fs = true;
                if (s == JOptionPane.NO_OPTION) {
                    fs = false;
                }
                new MainFrame(fs);
            }
        });//*/
    }

    /**
     * <pre>
     * 解構子
     * 解構 maun thread
     * 釋放 buffer strategy 資源
     * </pre>
     * @throws java.lang.Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        this.stop();
        this.mainThread = null;
        this.bufferStrategy.dispose();
        this.titleMenu = null;
        this.multiModeMenu = null;
        this.maze = null;
        this.bufferStrategy.dispose();
        this.bufferStrategy = null;
        this.keyListener = null;
        this.backstage = null;
        System.gc();
        super.finalize();
    }
}
