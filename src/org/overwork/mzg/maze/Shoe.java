package org.overwork.mzg.maze;

import java.awt.*;
import org.overwork.mzg.*;
import org.w3c.dom.*;

/**
 *
 * @author overing
 */
public class Shoe extends Item implements Runnable {
    static final int NumOfExist = 4;
    static final int DefActTime = 16;
    private int actionTime;
    private Thread action;

    public Shoe(int w, int h, Maze maker) {
        super(0, 0, w, h, maker);
        action = null;
    }

    protected void afterGet() {
        actionTime = DefActTime;
        action = new Thread(this);
        action.start();
    }

    protected void beforeLost() {
        //nothing to do
    }

    public void run() {
        while (actionTime-- > 0) {
            MainFrame.sleep(1000);
        }
        beLost();
    }

    public void paint(Graphics g) {
        Image image = ImageKit.ShoeImage.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
        g.drawImage(image, x, y, null);
    }

    protected void finalize() throws Throwable {
        this.actionTime = 0;
        this.action.interrupt();
        this.action = null;
        System.gc();
        super.finalize();
    }
//////////////////////////////////////////////////////////////////////////�ǿ��

    ////////////////////////////////////////////////////object to xml
    Element toXMLElement(Document doc) {
        Element eShoe = doc.createElement("Shoe");
        super.appendLayerData(eShoe);
        return eShoe;
    }

    public String toXMLString() {
        Document doc = XMLKit.createXMLDoc();

        Element eShoe = toXMLElement(doc);

        doc.appendChild(eShoe);
        return XMLKit.xmlDoc2String(doc);
    }

    ////////////////////////////////////////////////////xml to object
    public Shoe(Node n) {
        super(n);
    }
}
