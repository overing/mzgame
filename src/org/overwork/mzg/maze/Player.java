package org.overwork.mzg.maze;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.Vector;
import org.overwork.mzg.ImageKit;
import org.overwork.mzg.MainFrame;
import org.overwork.mzg.XMLKit;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * 此類別定義地圖元件之一 玩家
 * @author Overing Zero
 */
public class Player extends Component {

    /** 玩家的id 人物圖構成&多人連線辨識用 */
    public final int id;
    private final int imageIndex;
    /** 面向南（下）。 */
    public static final int DIR_SOUTH = 0;
    /** 面向西（左）。 */
    public static final int DIR_WEST = 1;
    /** 面向東（右）。 */
    public static final int DIR_EAST = 2;
    /** 面向北（上）。 */
    public static final int DIR_NORTH = 3;
    /** 玩家面對的方向(對應16宮格的y軸) */
    private int dir;
    /** 玩家的姿勢(對應16宮格的x軸) */
    private int pos;
    /** 視野半徑(像素) */
    int look;
    /** 移動速度(每次移動像素) */
    private int d;
    /** 持有的道具 */
    private final Vector<Item> items;
    private Image[][] imageMap;

    public Player(int id, int imgIdx, int w, int h, Maze mz) {
        super(0, 0, w, h, mz);
        this.id = id;
        imageIndex = imgIdx;

        dir = 0;
        pos = 0;
        look = (int) (height * 3.5);
        d = (int) (width * 0.3);
        items = new Vector<Item>();

        imageMap = ImageKit.getPlayerImageMap(imageIndex);
        for (int iy = 0; iy < imageMap.length; iy++) {
            for (int ix = 0; ix < imageMap[iy].length; ix++) {
                imageMap[iy][ix] = imageMap[iy][ix].getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
            }
        }
    }

    public Key hit(Player p) {
        synchronized (p) {
            Key k = p.getKeyRef();
            if (intersects(p)) {
                if (k != null) {
                    if (starVal >= p.starVal) {
                        k.beLost();
                        return k;
                    }
                }
            }
            return null;
        }
    }

    public void addItem(Item i) {
        synchronized (items) {
            if (items.add(i)) {
                if (Tip.class.isInstance(i)) {
                    tipVal += 1;
                }
                if (Star.class.isInstance(i)) {
                    starVal += 1;
                }
                if (Shoe.class.isInstance(i)) {
                    shoeVal += 1;
                }
                if (Key.class.isInstance(i)) {
                    keyVal = true;
                    look = (int) (height * 5);
                }
                //System.out.println("add:" + i);
            }
        }
    }

    public void removeItem(Item i) {
        synchronized (items) {
            if (items.remove(i)) {
                if (Tip.class.isInstance(i)) {
                    tipVal -= 1;
                }
                if (Star.class.isInstance(i)) {
                    starVal -= 1;
                }
                if (Shoe.class.isInstance(i)) {
                    shoeVal -= 1;
                }
                if (Key.class.isInstance(i)) {
                    keyVal = false;
                    look = (int) (height * 3.5);
                }
                //System.out.println("rm:" + i);
            }
        }
    }

    public int getD() {
        return d;
    }

    private Key getKeyRef() {
        synchronized (items) {
            for (Item i : items) {
                if (Key.class.isInstance(i)) {
                    return (Key) i;
                }
            }
            return null;
        }
    }

    public boolean hasKey() {
        return keyVal;
    }

    public int holdShoe() {
        synchronized (this) {
            return shoeVal;
        }
    }

    public Image getImage() {
        return ImageKit.getPlayerImageMap(imageIndex)[0][0];
    }
    private int tipVal;
    private int starVal;
    private int shoeVal;
    private boolean keyVal;
    private Ellipse2D.Float locaSpace;

    public Ellipse2D.Float getLookSpace() {
        if (locaSpace == null) {
            locaSpace = new Ellipse2D.Float((float) x - look + width / 2, (float) y - look + height / 2, (float) look * 2, (float) look * 2);
        } else {
            locaSpace.setFrame((float) x - look + width / 2, (float) y - look + height / 2, (float) look * 2, (float) look * 2);
        }
        return locaSpace;
    }

    /**
     * 設定玩家的方向<br>
     * 方向值可以是 DIR_SOUTH, DIR_WEST, DIR_EAST, DIR_NORTH
     * @param nDir 方向值
     */
    public void setDirection(int nDir) {
        dir = nDir;
    }

    /**
     * 設定玩家位置
     * @param x x
     * @param y y
     */
    public void setLocation(int x, int y) {
        if (y > super.y) {
            setDirection(Player.DIR_SOUTH);
        } else if (y < super.y) {
            setDirection(Player.DIR_NORTH);
        }
        if (x > super.x) {
            setDirection(Player.DIR_EAST);
        } else if (x < super.x) {
            setDirection(Player.DIR_WEST);
        }
        nextPose();
        super.setLocation(x, y);
    }

    /**
     * 改變玩家圖片到下一個姿勢
     */
    public void nextPose() {
        pos = (pos + 1) % 4;
    }

    public void paint(Graphics g) {
        Shape s = g.getClip();
        if(s != null && !s.intersects(this)) {
            return;
        }
        g.drawImage(imageMap[dir][pos], x, y, null);

        if (tipVal > 0) {
            g.setClip(getLookSpace());
            Rectangle tp = maker.getExitRectangle();
            if (tp == null) {
                tp = maker.getKeyRectangle();
            }

            int tx, ty, x1, y1;
            tx = tp.x + tp.width / 2;
            ty = tp.y + tp.height / 2;
            x1 = x + width / 2;
            y1 = y + height / 2;

            g.setColor(Color.BLUE);
            g.drawLine(x1, y1, tx, ty);
            //g.setClip(null);
        }//*/
    }

    @Override
    protected void finalize() throws Throwable {
        this.items.clear();
        System.gc();
        super.finalize();
    }
//////////////////////////////////////////////////////////////////////////傳輸用
    public static final String XMLTag_id = "id";
    public static final String XMLTag_imgIdx = "imgIdx";
    public static final String XMLTag_dir = "dir";
    public static final String XMLTag_pos = "pos";
    public static final String XMLTag_look = "look";
    public static final String XMLTag_tipVal = "tipVal";

    ////////////////////////////////////////////////////object to xml
    public String toXMLString() {
        Document doc = XMLKit.createXMLDoc();
        Element ePlayer = toXMLElement(doc);
        doc.appendChild(ePlayer);
        return XMLKit.xmlDoc2String(doc);
    }

    public Element toXMLElement(Document doc) {
        Element ePlayer = doc.createElement("Player");
        super.appendLayerData(ePlayer);
        ePlayer.setAttribute(XMLTag_id, id + "");
        ePlayer.setAttribute(XMLTag_imgIdx, imageIndex + "");
        ePlayer.setAttribute(XMLTag_dir, dir + "");
        ePlayer.setAttribute(XMLTag_pos, pos + "");
        ePlayer.setAttribute(XMLTag_look, look + "");
        ePlayer.setAttribute(XMLTag_tipVal, tipVal + "");
        return ePlayer;
    }

    ////////////////////////////////////////////////////xml to object
    public Player(Node n) {
        super(n);
        id = XMLKit.getAttribValue(n, XMLTag_id);
        imageIndex = XMLKit.getAttribValue(n, XMLTag_imgIdx);
        items = new Vector<Item>();

        dir = XMLKit.getAttribValue(n, XMLTag_dir);
        pos = XMLKit.getAttribValue(n, XMLTag_pos);
        look = XMLKit.getAttribValue(n, XMLTag_look);
        tipVal = XMLKit.getAttribValue(n, XMLTag_tipVal);

        imageMap = ImageKit.getPlayerImageMap(imageIndex);
        for (int iy = 0; iy < imageMap.length; iy++) {
            for (int ix = 0; ix < imageMap[iy].length; ix++) {
                imageMap[iy][ix] = imageMap[iy][ix].getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
            }
        }
    }

    public void updateFromXMLNode(Node np) {
        super.updateFromXMLNode(np);

        dir = XMLKit.getAttribValue(np, XMLTag_dir);
        pos = XMLKit.getAttribValue(np, XMLTag_pos);
        look = XMLKit.getAttribValue(np, XMLTag_look);
        tipVal = XMLKit.getAttribValue(np, XMLTag_tipVal);
    }
}
