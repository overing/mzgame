package org.overwork.mzg.maze;

import java.awt.*;
import org.overwork.mzg.*;
import org.w3c.dom.*;

/**
 *
 * @author overing
 */
public class Key extends Item {

    public Key(int w, int h, Maze maker) {
        super(0, 0, w, h, maker);
    }

    protected void afterGet() {
        maker.makeExit();
    }

    protected void beforeLost() {
        maker.removeExit();
    }

    public void paint(Graphics g) {
        Image image = ImageKit.KeyImage.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
        g.drawImage(image, x, y, null);
    }
//////////////////////////////////////////////////////////////////////////�ǿ��

    ////////////////////////////////////////////////////object to xml
    Element toXMLElement(Document doc) {
        Element eKey = doc.createElement("Key");
        super.appendLayerData(eKey);
        return eKey;
    }

    public String toXMLString() {
        Document doc = XMLKit.createXMLDoc();

        Element eKey = toXMLElement(doc);

        doc.appendChild(eKey);
        return XMLKit.xmlDoc2String(doc);
    }

    ////////////////////////////////////////////////////xml to object
    public Key(Node n) {
        super(n);
    }
}
