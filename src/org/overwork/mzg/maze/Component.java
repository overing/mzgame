package org.overwork.mzg.maze;

import java.awt.*;
import org.overwork.mzg.XMLKit;
import org.w3c.dom.*;

/**
 * 此類別定義地圖元件的抽象類別
 * @author Overing Zero
 */
public abstract class Component extends Rectangle {

    Maze maker;

    /**
     * 以位置及圖像構成地圖元件
     * @param x x
     * @param y y
     * @param w width
     * @param h height
     * @param maker 製造者(地圖物件)
     */
    public Component(int x, int y, int w, int h, Maze maker) {
        super(x, y, w, h);
        this.maker = maker;
    }

    /**
     * Get the value of mazeRef
     *
     * @return the value of mazeRef
     */
    public Maze getMazeRef() {
        return maker;
    }

    /**
     * Set the value of mazeRef
     *
     * @param mazeRef new value of mazeRef
     */
    void setMazeRef(Maze mazeRef) {
        this.maker = mazeRef;
    }

    public abstract void paint(Graphics g);

    protected void finalize() throws Throwable {
        this.maker = null;
        System.gc();
        super.finalize();
    }
//////////////////////////////////////////////////////////////////////////傳輸用
    static final String XMKTag_x = "x";
    static final String XMKTag_y = "y";
    static final String XMKTag_w = "w";
    static final String XMKTag_h = "h";

    ////////////////////////////////////////////////////object to xml
    public abstract String toXMLString();

    void appendLayerData(Element eLoc) {
        eLoc.setAttribute(XMKTag_x, x + "");
        eLoc.setAttribute(XMKTag_y, y + "");
        eLoc.setAttribute(XMKTag_w, width + "");
        eLoc.setAttribute(XMKTag_h, height + "");
    }

    ////////////////////////////////////////////////////xml to object
    public Component(Node n) {
        x = XMLKit.getAttribValue(n, XMKTag_x);
        y = XMLKit.getAttribValue(n, XMKTag_y);
        width = XMLKit.getAttribValue(n, XMKTag_w);
        height = XMLKit.getAttribValue(n, XMKTag_h);
    }

    void updateFromXMLNode(Node n) {
        x = XMLKit.getAttribValue(n, XMKTag_x);
        y = XMLKit.getAttribValue(n, XMKTag_y);
        width = XMLKit.getAttribValue(n, XMKTag_w);
        height = XMLKit.getAttribValue(n, XMKTag_h);
    }
}
