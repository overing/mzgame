package org.overwork.mzg.maze;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Vector;
import org.overwork.mzg.*;
import org.overwork.mzg.connection.Server;
import org.w3c.dom.*;

/**
 *
 * @author overing
 */
public class Maze {

    private Server server;
    private static Dimension LogSize = new Dimension(1600, 1200);
    private Dimension logGridSize;
    private Point logShift;
    private final boolean[][] e_table;
    private BufferedImage image_wallsLayer;
    private final Vector<Rectangle> walls = new Vector<Rectangle>();
    private final Vector<Item> items = new Vector<Item>();
    private final Vector<Player> players = new Vector<Player>();
    private Exit exit;

    public Maze(Server server, int rows, int cols) {
        this(rows, cols);
        this.server = server;
    }

    public Maze(int rows, int cols) {
        logGridSize = new Dimension(LogSize.width / rows, LogSize.height / cols);
        logShift = new Point(logGridSize.width / 8, logGridSize.height / 8);
        e_table = new boolean[cols * 2 + 1][rows * 2 + 1];
        image_wallsLayer = DisplayManager.createBufferedImage(LogSize.width, LogSize.height);

        int m = (logGridSize.width < logGridSize.height ? logGridSize.width : logGridSize.height);
        ImageKit.scalePlayerImage((int) ((double) m * 0.5));
    }

    public void newMap() {
        fillAll();
        int startX = (int) ((e_table[0].length / 2) * Math.random()) * 2 + 1;
        int startY = (int) ((e_table.length / 2) * Math.random()) * 2 + 1;
        e_table[startY][startX] = true;
        hewRood(startX, startY);
        randomRemove((float) 0.25);
        resetWalls();
        paintWalls();

        exit = null;

        //paint2Cons();
    }

    private void fillAll() {
        for (int y = 0; y < e_table.length; y++) {
            for (int x = 0; x < e_table[y].length; x++) {
                e_table[y][x] = false;
            }
        }
    }

    private void hewRood(int x, int y) {
        int m;
        boolean[] tried = {false, false, false, false}; //四個方向
        do {
            do { //隨機選一個還沒試過的方向開始挖
                m = (int) (4 * Math.random());
            } while (tried[m]);
            tried[m] = true; //標示該方向為已試
            switch (m) {
                case 0: //往左
                    if (x - 2 > 0) { //如果左邊格不是最外圍牆
                        if (e_table[y][x - 2] == false) { //如果左兩格不能通
                            e_table[y][x - 1] = true;//移除牆面
                            e_table[y][x - 2] = true;//移除牆面
                            hewRood(x - 2, y); //以左兩格為基準繼續挖
                        }
                    }
                    break;
                case 1: //往上
                    if (y - 2 > 0) { //如果上面格不是最外圍牆
                        if (e_table[y - 2][x] == false) { //如果上兩格不能通
                            e_table[y - 1][x] = true;
                            e_table[y - 2][x] = true;
                            hewRood(x, y - 2); //以上兩格為基準繼續挖
                        }
                    }
                    break;
                case 2: //往右
                    if (x + 2 < e_table[0].length - 1) { //如果右邊格不是最外圍牆
                        if (e_table[y][x + 2] == false) {
                            e_table[y][x + 1] = true;
                            e_table[y][x + 2] = true;
                            hewRood(x + 2, y); //以右兩格為基準繼續挖
                        }
                    }
                    break;
                case 3: //往下
                    if (y + 2 < e_table.length - 1) { //如果下面格不是最外圍牆
                        if (e_table[y + 2][x] == false) {
                            e_table[y + 1][x] = true;
                            e_table[y + 2][x] = true;
                            hewRood(x, y + 2); //以下兩格為基準繼續挖
                        }
                    }
            }
        } while (!(tried[0] && tried[1] && tried[2] && tried[3]));
    }

    private void randomRemove(float prt) {
        resetWalls();
        int x, y, nor = (int) (walls.size() * prt);
        synchronized (e_table) {
            while (nor > 0) {
                x = (int) ((e_table[0].length - 2) * Math.random()) + 1;
                if (x % 2 == 0) {
                    y = (int) ((e_table.length - 2) * Math.random()) + 1;
                } else {
                    y = (int) (((int) (e_table.length / 2) - 1) * Math.random() + 1) * 2;
                }
                if (e_table[y][x] == false) {
                    e_table[y][x] = true;
                    nor--;
                }
            }
        }
    }

    private void resetWalls() {
        Rectangle newWall;
        synchronized (e_table) {
            synchronized (walls) {
                walls.clear();
                for (int y = 1; y <= e_table.length; y += 2) {
                    for (int x = 1; x <= e_table[0].length; x += 2) {
                        if (y < e_table.length) {
                            if ((x == 1) ||
                                    (x > 1 && x < e_table[0].length && e_table[y][x - 1] == false) ||
                                    (x == e_table[0].length)) {
                                newWall = new Rectangle(
                                        (x / 2) * logGridSize.width - (logShift.x / 2),
                                        (y / 2) * logGridSize.height - (logShift.y / 2),
                                        logShift.x,
                                        logGridSize.height + logShift.y);
                                walls.add(newWall);
                            }
                        }

                        if (x < e_table[0].length) {
                            if ((y == 1) ||
                                    (y > 1 && y < e_table.length && e_table[y - 1][x] == false) ||
                                    (y == e_table.length)) {
                                newWall = new Rectangle(
                                        (x / 2) * logGridSize.width - (logShift.x / 2),
                                        (y / 2) * logGridSize.height - (logShift.y / 2),
                                        logGridSize.width + logShift.x,
                                        logShift.y);
                                walls.add(newWall);
                            }
                        }
                    }
                }
            }
        }
    }

    public Vector<Rectangle> getSpaceGrids() {
        //蒐集e_table的奇數格
        Vector<Rectangle> rval = new Vector<Rectangle>();
        for (int y = 1; y < e_table.length / 2; y += 2) {
            for (int x = 1; x < e_table[0].length / 2; x += 2) {
                rval.add(new Rectangle(
                        x * logGridSize.width,
                        y * logGridSize.height,
                        logGridSize.width,
                        logGridSize.height));
            }
        }
        //移除出口位置
        Rectangle rr = null;
        if (exit != null) {
            rm_exit:
            for (Rectangle e : rval) {
                if (e.intersects(exit)) {
                    rr = e;
                    break rm_exit;
                }
            }
            rval.remove(rr);
        }
        //移除已有道具的位置
        Vector<Rectangle> rvr = new Vector<Rectangle>();
        for (Rectangle e : rval) {
            for (Rectangle i : items) {
                if (e.intersects(i)) {
                    rvr.add(e);
                }
            }
        }
        rval.removeAll(rvr);
        //移除玩家所在位置
        rvr.clear();
        for (Rectangle e : rval) {
            for (Rectangle p : players) {
                if (e.intersects(p)) {
                    rvr.add(e);
                }
            }
        }
        rval.removeAll(rvr);
        //
        return rval;
    }

    public Vector<Rectangle> getWalls() {
        return walls;
    }

    public void makeItems() {
        synchronized (this) {
            int iw = logGridSize.width / 2;
            int ih = logGridSize.height / 2;
            reMakeItem(new Key(iw, ih, this));
            for (int i = 0; i < Star.NumOfExist; i++) {
                reMakeItem(new Star(iw, ih, this));
            }
            for (int i = 0; i < Shoe.NumOfExist; i++) {
                reMakeItem(new Shoe(iw, ih, this));
            }
            for (int i = 0; i < Tip.NumOfExist; i++) {
                reMakeItem(new Tip(iw, ih, this));
            }
        }
    }

    public synchronized void makeExit() {
        Vector<Rectangle> sg = getSpaceGrids();
        Rectangle sp = sg.get((int) (sg.size() * Math.random()));
        exit = new Exit(sp.x + 1, sp.y + 1, sp.width - 2, sp.height - 2, this);
        if (server != null) {
            server.syncExit(exit.toXMLString());
        }
        sp = null;
        sg = null;//*/
    }

    public void removeExit() {
        exit = null;
        if (server != null) {
            server.syncExit(null);
        }
    }

    public synchronized void reMakeItem(Item i) {
        Vector<Rectangle> sg = getSpaceGrids();
        Rectangle sp = sg.get((int) (sg.size() * Math.random()));
        if (items.indexOf(i) == -1) {
            items.add(i);
        }
        setALocationRelativeToB(i, sp);
        if (server != null) {
            server.syncItem(i.toXMLString());
        }
        sp = null;
        sg = null;
    }

    private synchronized void addItem(Item i) {
        items.add(i);
    }

    public Rectangle getExitRectangle() {
        return exit;
    }

    public Rectangle getKeyRectangle() {
        for (Item i : items) {
            if (Key.class.isInstance(i)) {
                return i;
            }
        }
        return null;
    }

    public Vector<Item> getItems() {
        return items;
    }

    public synchronized Player addPlayer(int imageIdx) {
        if (players.size() >= 5 || imageIdx == -1) {
            return null;
        } else {
            Rectangle s = new Rectangle(0, 0, logGridSize.width, logGridSize.height);
            Player np = null;
            np = new Player(players.size(), imageIdx, logGridSize.width / 3, logGridSize.height / 2, this);
            switch (players.size()) {
                case 0:
                    s.setLocation(0, 0);
                    break;
                case 1:
                    s.setLocation(LogSize.width - logGridSize.width, LogSize.height - logGridSize.height);
                    break;
                case 2:
                    s.setLocation(0, LogSize.height - logGridSize.height);
                    break;
                case 3:
                    s.setLocation(LogSize.width - logGridSize.width, 0);
                    break;
                case 4:
                    s.setLocation(LogSize.width - logGridSize.width, LogSize.height - logGridSize.height);
                    break;
            }
            setALocationRelativeToB(np, s);
            players.add(np);
            return np;
        }
    }

    public void removePlayer(int recid) {
        //
    }

    private synchronized void addPlayer(Player p) {
        players.add(p);
    }

    public Player getPlayer(int index) {
        return players.get(index);
    }

    private boolean tryMove(Rectangle np) {
        for (Rectangle wr : walls) {
            if (wr.intersects(np)) {
                return false;
            }
        }
        return true;
    }

    public boolean movePlayer(int mxp, int myp, int playerIndex) {
        boolean rval = false;
        Player p = getPlayer(playerIndex);
        int md = (int) (p.getD() * (p.hasKey() ? 0.8 : 1) * (1 + p.holdShoe() * 0.34));
        int mx, my;
        Rectangle np;

        mx = md * mxp;
        my = md * myp;
        np = new Rectangle(p.x + mx, p.y + my, p.width, p.height);
        if (tryMove(np)) {
            rval = true;
        } else {
            if (mxp != 0 && myp != 0) {
                int orgmxp = mxp;

                mxp = 0;
                mx = md * mxp;
                my = md * myp;
                np.setLocation(p.x + mx, p.y + my);
                if (tryMove(np)) {
                    rval = true;
                } else {
                    mxp = orgmxp;
                    myp = 0;
                    mx = md * mxp;
                    my = md * myp;
                    np.setLocation(p.x + mx, p.y + my);
                    if (tryMove(np)) {
                        rval = true;
                    }
                }
            }
        }
        if (rval) {
            p.setLocation(p.x + mx, p.y + my);
            checkItem(p);
            synchronized (players) {
                for (Player op : players) {
                    if (op != p) {
                        Key k = p.hit(op);
                        if(k != null && server != null) {
                            server.syncPlayer(op.toXMLString());
                            server.syncItem(k.toXMLString());
                        }
                    }
                }
            }
        }

        return rval;
    }

    private void checkItem(Player p) {
        synchronized (items) {
            for (Item i : items) {
                if (p.intersects(i)) {
                    i.forPlayer(p);
                    if (server != null) {
                        server.syncItem(i.toXMLString());
                    }
                }
            }
        }
    }

    public void paint(Graphics og, int phyWidth, int phyHeight, Shape[] clips, String[] names) {
        Graphics2D g = (Graphics2D) og;
        double sx = ((double) phyWidth) / ((double) LogSize.width);
        double sy = ((double) phyHeight) / ((double) LogSize.height);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.scale(sx, sy);//*/

        if (clips == null || clips.length == 0) {
            g.drawImage(image_wallsLayer, 0, 0, null);
            paintItems(g, null);
            paintExit(g, null);
            painrPlayers(g, names);
        } else {
            for (Shape s : clips) {
                g.setClip(s);
                g.drawImage(image_wallsLayer, 0, 0, null);
                paintItems(g, s);
                paintExit(g, s);
                painrPlayers(g, names);
                g.setClip(null);
            }
        }
    }

    private void paintWalls() {
        Color wallcolor = Color.GRAY, background = Color.WHITE;
        BufferedImage tmpImg = DisplayManager.createBufferedImage(LogSize.width, LogSize.height);
        Graphics tmpG = tmpImg.createGraphics();
        tmpG.setColor(background);
        tmpG.fillRect(0, 0, LogSize.width, LogSize.height);
        tmpG.setColor(wallcolor);
        synchronized (walls) {
            for (Rectangle w : walls) {
                //tmpG.fill3DRect(w.x, w.y, w.width, w.height, true);
                tmpG.fillRect(w.x, w.y, w.width, w.height);
            }
        }
        Graphics g = image_wallsLayer.createGraphics();
        g.drawImage(tmpImg.getScaledInstance(LogSize.width, LogSize.height, Image.SCALE_SMOOTH), 0, 0, null);
        g.dispose();
        tmpG.dispose();
    }

    private void paintItems(Graphics g, Shape clip) {
        synchronized (items) {
            for (Item i : items) {
                if ((clip == null) ? true : clip.intersects(i) && i.x > 0 && i.y > 0) {
                    i.paint(g);
                }
            }
        }
    }

    private void paintExit(Graphics g, Shape clip) {
        if (exit != null) {
            if ((clip == null) ? true : clip.intersects(exit)) {
                exit.paint(g);
            }
        }
    }

    private void painrPlayers(Graphics g, String[] names) {
        for (Player p : players) {
            /*if(names != null) {
                String name = names[p.id];
                g.setFont(g.getFont().deriveFont((float)24));
                int sw = g.getFontMetrics().stringWidth(name);
                g.drawString(name, p.x - (p.width + sw) / 2, p.y);
            }//*/
            p.paint(g);
        }
    }

    public void paint2Cons() {
        System.out.println();
        for (int y = 0; y < e_table.length; y++) {
            for (int x = 0; x < e_table[y].length; x++) {
                System.out.print(e_table[y][x] ? "　" : "國");
            }
            System.out.println();
        }
    }

    public static void setALocationRelativeToB(Rectangle a, Rectangle b) {
        a.x = b.x + (b.width - a.width) / 2;
        a.y = b.y + (b.height - a.height) / 2;
    }

    @Override
    protected void finalize() throws Throwable {
        this.walls.clear();
        this.items.clear();
        this.exit = null;
        System.gc();
        super.finalize();
    }

    public boolean chechExit(Player p) {
        if (exit != null && p.hasKey()) {
            return exit.contains(p);
        } else {
            return false;
        }
    }

//////////////////////////////////////////////////////////////////////////////////////////////傳輸用成員
    public String toXMLString() {
        Document doc = XMLKit.createXMLDoc();

        Element eMaze = doc.createElement("Maze");
        eMaze.setAttribute("rows", e_table[0].length + "");
        eMaze.setAttribute("cols", e_table.length + "");

        Element eWall;
        for (Rectangle w : walls) {
            eWall = doc.createElement("Wall");
            eWall.setAttribute("x", w.x + "");
            eWall.setAttribute("y", w.y + "");
            eWall.setAttribute("w", w.width + "");
            eWall.setAttribute("h", w.height + "");
            eMaze.appendChild(eWall);
        }

        for (Item i : items) {
            eMaze.appendChild(i.toXMLElement(doc));
        }

        for (Player p : players) {
            eMaze.appendChild(p.toXMLElement(doc));
        }

        doc.appendChild(eMaze);
        return XMLKit.xmlDoc2String(doc);
    }

    public static Maze fromXMLString(String xmlString) {
        Maze rval;

        Document doc = XMLKit.string2XML(xmlString);
        Node nMaze = doc.getElementsByTagName("Maze").item(0);

        int rows = XMLKit.getAttribValue(nMaze, "rows");
        int cols = XMLKit.getAttribValue(nMaze, "cols");
        rval = new Maze(rows, cols);

        NodeList nlMaze = nMaze.getChildNodes();
        for (int i = 0; i < nlMaze.getLength(); i++) {
            Node n = nlMaze.item(i);
            if (n.getNodeName().equals("Wall")) {
                int x = XMLKit.getAttribValue(n, "x");
                int y = XMLKit.getAttribValue(n, "y");
                int w = XMLKit.getAttribValue(n, "w");
                int h = XMLKit.getAttribValue(n, "h");
                rval.walls.add(new Rectangle(x, y, w, h));
            } else if (n.getNodeName().equals("Player")) {
                Player p = new Player(n);
                p.setMazeRef(rval);
                rval.addPlayer(p);
            } else if (Item.isItemNode(n)) {
                rval.addItem(Item.fromXMLNode(n));
            }
        }
        rval.paintWalls();

        return rval;
    }

    public void updateItem(String xmlString) {
        Document doc = XMLKit.string2XML(xmlString);
        Node ni = doc.getChildNodes().item(0);
        Item i = Item.fromXMLNode(ni);
        synchronized (items) {
            for (Item ri : items) {
                if (ri.id == i.id) {
                    ri.updateFromXMLNode(ni);
                    return;
                }
            }
        }
    }

    public void updatePlayer(String xmlString) {
        Document doc = XMLKit.string2XML(xmlString);
        Node np = doc.getElementsByTagName("Player").item(0);
        int rid = XMLKit.getAttribValue(np, Player.XMLTag_id);
        getPlayer(rid).updateFromXMLNode(np);
    }

    public void updateExit(String xmlString) {
        if (xmlString != null) {
            Document doc = XMLKit.string2XML(xmlString);
            Node ne = doc.getElementsByTagName("Exit").item(0);
            exit = new Exit(ne);
        } else {
            exit = null;
        }
    }
}
