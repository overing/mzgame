package org.overwork.mzg.maze;

import java.awt.*;
import org.overwork.mzg.*;
import org.w3c.dom.*;

/**
 *
 * @author overing
 */
public class Exit extends Component {

    public Exit(int x, int y, int w, int h, Maze m) {
        super(x, y, w, h, m);
    }

    public void paint(Graphics g) {
        Image image = ImageKit.ExitImage.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
        g.drawImage(image, x, y, null);
    }

//////////////////////////////////////////////////////////////////////////�ǿ��
    
    ////////////////////////////////////////////////////object to xml
    public String toXMLString() {
        Document doc = XMLKit.createXMLDoc();

        Element eExit = doc.createElement("Exit");
        super.appendLayerData(eExit);

        doc.appendChild(eExit);
        return XMLKit.xmlDoc2String(doc);
    }

    ////////////////////////////////////////////////////xml to object
    public Exit(Node n) {
        super(n);
    }
}
