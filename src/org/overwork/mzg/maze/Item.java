package org.overwork.mzg.maze;

import org.overwork.mzg.XMLKit;
import org.w3c.dom.*;

/**
 *
 * @author overing
 */
public abstract class Item extends Component {

    static int count;
    int id;
    Player own;

    public Item(int x, int y, int w, int h, Maze maker) {
        super(x, y, w, h, maker);
        id = ++Item.count;
        own = null;
    }

    public synchronized void forPlayer(Player newown) {
        x = -width;
        y = -height;
        own = newown;
        own.addItem(this);
        afterGet();
    }

    protected abstract void afterGet();

    public synchronized void beLost() {
        beforeLost();
        own.removeItem(this);
        own = null;
        maker.reMakeItem(this);
    }

    protected abstract void beforeLost();

    protected void finalize() throws Throwable {
        System.gc();
        super.finalize();
    }
//////////////////////////////////////////////////////////////////////////�ǿ��
    static final String XMLTag_id = "id";

    static boolean isItemNode(Node n) {
        String nodeName = n.getNodeName();
        if (nodeName.equals("Key") ||
                nodeName.equals("Shoe") ||
                nodeName.equals("Star") ||
                nodeName.equals("Tip")) {
            return true;
        }
        return false;
    }

    ////////////////////////////////////////////////////object to xml
    public abstract String toXMLString();

    abstract Element toXMLElement(Document doc);

    void appendLayerData(Element eLoc) {
        super.appendLayerData(eLoc);
        eLoc.setAttribute(XMLTag_id, id + "");
    }

    ////////////////////////////////////////////////////xml to object
    public Item(Node n) {
        super(n);

        id = XMLKit.getAttribValue(n, XMLTag_id);
    }

    public static Item fromXMLNode(Node n) {
        Item rval = null;
        String nodeName = n.getNodeName();
        if (nodeName.equals("Key")) {
            rval = new Key(n);
        } else if (nodeName.equals("Shoe")) {
            rval = new Shoe(n);
        } else if (nodeName.equals("Star")) {
            rval = new Star(n);
        } else if (nodeName.equals("Tip")) {
            rval = new Tip(n);
        }
        return rval;
    }

    void updateFromXMLNode(Node n) {
        super.updateFromXMLNode(n);

        id = XMLKit.getAttribValue(n, XMLTag_id);
    }
}
