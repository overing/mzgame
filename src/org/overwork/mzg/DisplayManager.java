package org.overwork.mzg;

import java.awt.DisplayMode;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Window;
import java.awt.image.BufferedImage;

/**
 * 收納整合 java.awt.GraphicsEnvironment 類別<br/>
 * 及 java.awt.GraphicsDevice 類別的一些副程式<br/>
 * 提供簡便的方式將視窗類別 java.awt.Window 容器類別<br/>
 * 及其子類別容器(Frame, JWindow, JFrame等)<br/>
 * 在全螢幕模式與窗模式間切換<p/>
 *
 * 注意:應避免呼叫一些常用的視窗副程式<br/>
 *      show()<br/>
 *      setVisible(true)<br/>
 * 因為當設定成全螢幕時並不希望圖形由jvm來進行處理與重繪<br/>
 *
 * <hr>
 *
 * //全螢幕/視窗模式設定範例：<br/>
 * JFrame frame = new JFrame("Full Screen Demo");<p/>
 * 
 * //以最大解析度將frame視窗設定成全螢幕模式<br/>
 * DisplayManager.setFullScreen(frame);<p/>
 * 
 * //將frame視窗設定為寬480像素高320像素的視窗模式<br/>
 * DisplayManager.setWindowMode(frame, 480, 320);<br/>
 *
 * <hr>
 *
 * 由於設定成全螢幕模式的視窗顯示區域不再是原來paint(Graphics)方法所接受的繪圖對象<br/>
 * 因此便無法以複寫paint(Graphics)方法對傳入的Graphics對象進行繪圖來繪畫視窗外觀<br/>
 * 必須由使用視窗的createBufferStrategy(int)方法由該視窗物件管理的建立緩衝區<br/>
 * 接著呼叫getBufferStrategy()方法取得BufferStrategy物件<br/>
 * 再由BufferStrategy物件的getDrawGraphics()方法取出可供繪圖的Graphics對象<br/>
 * 進行全螢幕時的顯示繪製可以減少閃的情況發生<br/>
 * 當繪製圖形完畢需要以BufferStrategy物件的show()方法將緩衝區的影像顯示在螢幕上<p/>
 *
 * //全螢幕時的繪圖範例：<br/>
 * class MyFrame extends JFrame {<br/>
 *     BufferedImage backstage;<br/>
 *     BufferStrategy bufferStrategy;<p/>
 *
 *     public MyFrame() {<br/>
 *         DisplayManager.setFullScreen(frame); //將視窗設定成全螢幕顯示<br/>
 *         createBufferStrategy(2); //建立雙重緩衝<br/>
 *         bufferStrategy = getBufferStrategy(); //取得緩衝<br/>
 *     }<p/>
 *
 *     public void paint(Graphics g) {<br/>
 *         try {<br/>
 *             // 取得前景緩衝繪圖物件<br/>
 *             Graphics bufferGround = bufferStrategy.getDrawGraphics();<p/>
 *
 *             // 建立第二張畫布(後台)<br/>
 *             backstage = DisplayManager.createBufferedImage(getWidth(), getHeight());<p/>
 *
 *             // 取得第二畫布的繪圖物件<br/>
 *             Graphics backGraphics = backstage.createGraphics();<p/>
 *
 *             // 要畫東西直接畫在第二張畫布上<br/>
 *             backGraphics.draw...<br/>
 *             backGraphics.fill...<br/>
 *             backGraphics.drawString("test text", 10, 10);<br/>
 *             ...<p/>
 *
 *             // 要畫的都畫完了再一次貼到前景<br/>
 *             bufferGround.drawImage(backstage, 0, 0, null);<p/>
 *
 *             // 顯示影像<br/>
 *             bufferStrategy.show();<br/>
 *         } finally {<br/>
 *             // 釋放緩衝用掉的資源<br/>
 *             bufferStrategy.dispose();<br/>
 *         }<br/>
 *     }<br/>
 * }<br/>
 *
 * <hr/>
 *
 * Download：<br/>
 * <A HREF="DisplayManager.class">class file</A><br/>
 * <A HREF="DisplayManager.java">source file</A><p/>
 * @author Overing Zero
 * @see <A HREF="http://java.sun.com/javase/6/docs/api/java/awt/GraphicsEnvironment.html" title="class in java.awt">GraphicsEnvironment</A>
 * @see <A HREF="http://java.sun.com/javase/6/docs/api/java/awt/GraphicsDevice.html" title="class in java.awt">GraphicsDevice</A>
 * @see <A HREF="http://java.sun.com/javase/6/docs/api/java/awt/Window.html" title="class in java.awt">Window</A>
 * @see <A HREF="http://java.sun.com/javase/6/docs/api/java/awt/Frame.html" title="class in java.awt">Frame</A>
 * @see <A HREF="http://java.sun.com/javase/6/docs/api/java/awt/DisplayMode.html" title="class in java.awt">DisplayMode</A>
 * @see <A HREF="http://java.sun.com/javase/6/docs/api/javax/swing/JWindow.html" title="class in javax.swing">JWindow</A>
 * @see <A HREF="http://java.sun.com/javase/6/docs/api/javax/swing/JFrame.html" title="class in javax.swing">JFrame</A>
 * @version 1.4
 */
public class DisplayManager {

    /** Class 版本訊息 */
    public static final String VERSION = "release 1.4 (090625) by Overing Zero";
    /** 系統預設的繪圖裝置(通常指主要的顯示螢幕) */
    public static final GraphicsDevice GRAPHICES_DEVICE;
    /** 螢幕的預設繪圖描述 */
    public static final GraphicsConfiguration GRAPHICES_CONFIGURATION;
    private static boolean fullScreenMode;                           //正在使用的模式

    static { //取得繪圖裝置訊息
        GRAPHICES_DEVICE = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        GRAPHICES_CONFIGURATION = GRAPHICES_DEVICE.getDefaultConfiguration();
        fullScreenMode = false;
    }

    /**
     * 將傳入的視窗物件以最大解析度設定成全螢幕模式<br>
     * @param window 要操作的視窗物件
     * @return 系統繪圖環境可用的最大解析度顯示模式
     */
    public static DisplayMode setFullScreen(Window window) {
        DisplayMode rval = getMAXDisplayMode();
        DisplayManager.setFullScreen(window, rval);
        return rval;
    }

    /**
     * 將傳入的視窗物件以指定的解析度設定成全螢幕模式,<br>
     * 並回傳管理的視窗物件在全螢幕模式的繪圖緩衝<br>
     *
     * @param window 要操作的視窗物件
     * @param displayMode 指定的解析度模式
     */
    public synchronized static void setFullScreen(Window window, DisplayMode displayMode) {
        window.dispose();

        if (window instanceof Frame) {
            ((Frame) window).setExtendedState(Frame.MAXIMIZED_BOTH);
            ((Frame) window).setUndecorated(true);
        }

        window.setAlwaysOnTop(true);
        window.setIgnoreRepaint(true);
        //移除視窗邊框


        if (GRAPHICES_DEVICE.isFullScreenSupported()) {
            GRAPHICES_DEVICE.setFullScreenWindow(null);

            GRAPHICES_DEVICE.setFullScreenWindow(window);
            if (GRAPHICES_DEVICE.isDisplayChangeSupported()) {
                GRAPHICES_DEVICE.setDisplayMode(displayMode);
            }

            window.setSize(GRAPHICES_DEVICE.getFullScreenWindow().getWidth(),
                    GRAPHICES_DEVICE.getFullScreenWindow().getHeight());

            DisplayManager.fullScreenMode = true;
        } else {
            setWindowMode(window);
            System.err.println("not supported Full Screen!");
        }
    }

    /**
     * 將傳入的視窗物件依照視窗本身的長度與高度設定為視窗模式<br>
     * @param window 要操作的視窗物件
     */
    public static void setWindowMode(Window window) {
        DisplayManager.setWindowMode(window, window.getWidth(), window.getHeight());
    }

    /**
     * 將傳入的視窗物件以指定的寬度與高度設定成視窗模式
     * @param window 要操作的視窗物件
     * @param width 視窗寬度
     * @param height 視窗高度
     */
    public synchronized static void setWindowMode(Window window, int width, int height) {
        window.dispose();

        if (Frame.class.isInstance(window)) {
            ((Frame) window).setExtendedState(Frame.NORMAL);
        }

        window.setAlwaysOnTop(false);
        window.setIgnoreRepaint(false);

        DisplayManager.GRAPHICES_DEVICE.setFullScreenWindow(null);

        window.setVisible(true);

        window.setSize(width + window.getInsets().left + window.getInsets().right,
                height + window.getInsets().top + window.getInsets().bottom);
        window.setPreferredSize(window.getSize());

        window.setVisible(true);

        window.setLocationRelativeTo(null);

        DisplayManager.fullScreenMode = false;
    }

    /**
     * 回傳現時視窗是否為全螢幕模式
     * @return 管理的視窗物件是否為全螢幕模式
     */
    public static boolean isFullScreenMode() {
        return DisplayManager.fullScreenMode;
    }

    /**
     * 回傳系統繪圖環境可用的所有顯示模式
     * @return 系統繪圖環境可用的所有顯示模式
     */
    public static DisplayMode[] getDisplayModes() {
        return DisplayManager.GRAPHICES_DEVICE.getDisplayModes();
    }

    /**
     * 回傳系統繪圖環境可用的最大解析度顯示模式
     * @return 系統繪圖環境可用的最大解析度顯示模式
     */
    public static DisplayMode getMAXDisplayMode() {
        DisplayMode rval = GRAPHICES_DEVICE.getDisplayModes()[0];

        for (DisplayMode dm : GRAPHICES_DEVICE.getDisplayModes()) {
            if (dm.getHeight() * dm.getWidth() >= rval.getHeight() * rval.getWidth()) //最大長寬解析度
            {
                if (dm.getBitDepth() >= rval.getBitDepth()) //最多顯示色彩
                {
                    rval = dm;
                }
            }
        }
        //System.out.println("max dpi:" + rval.getWidth() + "x" + rval.getHeight() + ", refresh rate:" + rval.getRefreshRate() + ", bit depth:" + rval.getBitDepth());

        return rval;
    }

    /**
     * 透過本類別內含的 GraphicsConfiguration(系統預設) 產生緩衝影像
     * @param width 要產生的影像寬
     * @param height 要產生的影像高
     * @return 生成的緩衝影像物件
     */
    public static BufferedImage createBufferedImage(int width, int height) {
        return DisplayManager.GRAPHICES_CONFIGURATION.createCompatibleImage(width, height);
    }

    /**
     * 透過本類別內含的 GraphicsConfiguration(系統預設) 產生緩衝影像
     * @param gridWidth 要產生的影像寬
     * @param gridHeight 要產生的影像高
     * @param TRANSLUCENT  要產生的影像透明度設定
     * @return 生成的緩衝影像物件
     */
    public static BufferedImage createBufferedImage(int gridWidth, int gridHeight, int TRANSLUCENT) {
        return DisplayManager.GRAPHICES_CONFIGURATION.createCompatibleImage(gridWidth, gridHeight, TRANSLUCENT);
    }
}
