
package org.overwork.mzg;

import java.awt.event.*;

/**
 * <pre>
 * 此類別改寫自老師給的 game_key 類別
 * 用來偵測鍵盤鍵位狀態的類別
 * [Ctrl]+[F1]:this.isDown(KeyEvent.VK_CONTROL) && this.isDownOnce(KeyEvent.VK_F1)
 * </pre>
 * @author Overing Zero
 */
public class KeyListener extends KeyAdapter {

    private int keyCount;
    private boolean[] currentKeys;
    private KeyState[] keyState;
    private MainFrame mfRef;

    /**
     * 建立一個監聽鍵盤前 512 個鍵位變化的鍵盤事件監聽者
     */
    public KeyListener(MainFrame mfRef) {
        this(1024, mfRef);
    }

    /**
     * 建立一個監聽指定數量鍵位變化的鍵盤事件監聽者
     * @param keyCount 指定要監聽鍵位變化數量(必續大於 10)
     */
    public KeyListener(int keyCount, MainFrame mfRef) {
        if (keyCount > 10) {
            this.keyCount = keyCount;
        } else {
            throw new IllegalArgumentException("監聽的鍵位值錯誤! 值必須大於 10 而傳入的值是 " + keyCount);
        }
        this.mfRef = mfRef;
        this.currentKeys = new boolean[this.keyCount];
        this.keyState = new KeyState[this.keyCount];

        for (int i = 0; i < this.keyState.length; i++) {
            this.keyState[i] = KeyState.RELEASED;
        }
    }

    /**
     * 傳回指定鍵位的狀態是否為"按住"
     * @param keyCode 指定的鍵位值
     * @return 指定鍵位的狀態是否為"按住"
     */
    public synchronized boolean isDown(int keyCode) {
        if (keyCode > this.keyCount) {
            throw new IllegalArgumentException("鍵位值錯誤! 大於設定監聽的鍵位種類 " + this.keyCount + " 而傳入的值是 " + keyCode);
        }
        return ((this.keyState[keyCode] == KeyState.ONCE) || (this.keyState[keyCode] == KeyState.PRESSED));
    }

    /**
     * 傳回指定鍵位的狀態是否為"按下" (按了又立刻放開)
     * @param keyCode 指定的鍵位值
     * @return 指定鍵位的狀態是否為"按下" (按了又立刻放開)
     */
    public synchronized boolean isDownOnce(int keyCode) {
        if (keyCode > this.keyCount) {
            throw new IllegalArgumentException("鍵位值錯誤! 大於設定監聽的鍵位種類 " + this.keyCount + " 而傳入的值是 " + keyCode);
        }
        return (this.keyState[keyCode] == KeyState.ONCE);
    }

    /**
     * 輪尋檢查鍵位狀態
     */
    public synchronized void poll() {
        for (int i = 0; i < keyCount; i++) {
            if (this.currentKeys[i]) {
                if (this.keyState[i] == KeyState.RELEASED) {
                    this.keyState[i] = KeyState.ONCE;
                } else {
                    this.keyState[i] = KeyState.PRESSED;
                }
            } else {
                this.keyState[i] = KeyState.RELEASED;
            }
        }
    }

    @Override
    public synchronized void keyPressed(KeyEvent e) {
        mfRef.keyPressed(e);
        int keyCode = e.getKeyCode();
        if ((keyCode >= 0) && (keyCode < this.keyCount)) {
            this.currentKeys[keyCode] = true;
        }
    }

    @Override
    public synchronized void keyReleased(KeyEvent e) {
        mfRef.keyReleased(e);
        int keyCode = e.getKeyCode();
        if ((keyCode >= 0) && (keyCode < this.keyCount)) {
            this.currentKeys[keyCode] = false;
        }
    }

    private static enum KeyState {

        RELEASED,
        PRESSED,
        ONCE
    }

    @Override
    protected void finalize() throws Throwable {
        this.currentKeys = null;
        this.keyState = null;
        System.gc();
        super.finalize();
    }
}
