package org.overwork.mzg.connection;

import org.overwork.mzg.MainFrame;
import org.overwork.mzg.maze.Maze;
import org.overwork.mzg.maze.Player;

/**
 *
 * @author overing
 */
public class Server implements Receiver {

    public static enum State {

        INIT,
        WAIT_JOIN,
        PROCEEDING;

        public String toString() {
            return this.name();
        }
    }
    /** 預設群播位置 */
    public static final String LIST_GROUP_ADDRESS = "224.0.0.0";
    /** 預設 使用的本地 PORT */
    public static final int LIST_PORT = 8080;
    /** 預設 使用的本地 PORT */
    public static final int GAME_PORT = 8081;
    private final MCT listThread;
    private final MCT gameThread;
    private State state;
    private String gameGroupAddress;
    private boolean[] usedGroupNumber;
    private int numberOfPlayer;
    private int[] playersImageIndex;
    private String[] playersName;
    private String gameName;
    private final int[] clientsLiveingTime;
    private Thread clientsTimer;
    private MainFrame mfRef;
    private Maze maze;

    public Server(MainFrame mfRef) {
        this.mfRef = mfRef;
        gameName = mfRef.getClientName() + "'s GAME";
        listThread = new MCT(MCT.Type.LIST, LIST_GROUP_ADDRESS, LIST_PORT, this);
        while (!listThread.ready) {
        }
        gameGroupAddress = "224.0.0.0";
        usedGroupNumber = new boolean[255];
        usedGroupNumber[0] = true;
        clientsLiveingTime = new int[5];
        ////////////////////////////////init()
        state = State.INIT;
        listThread.send(Packet.SEARCH_GAME, LIST_PORT);
        MainFrame.sleep(500);
        //
        for (int i = 1; i < usedGroupNumber.length; i++) {
            if (usedGroupNumber[i] == false) {
                gameGroupAddress = "224.0.0." + i;
                break;
            }
        }
        //
        gameThread = new MCT(MCT.Type.GAME, gameGroupAddress, GAME_PORT, this);
        while (!listThread.ready) {
        }
        clientsTimer = new Thread() {

            public void run() {
                while (gameThread != null) {
                    synchronized (clientsLiveingTime) {
                        for (int i = 0; i < clientsLiveingTime.length; i++) {
                            if (clientsLiveingTime[i] > 0) {
                                clientsLiveingTime[i]--;
                            } else {
                                gameThread.send(Packet.SYNC_PLAYER_INFO.setData(i + Packet.SS + "" + Packet.SS + "-1"), Client.GAME_PORT);
                                numberOfPlayer--;
                            }
                        }
                    }
                    MainFrame.sleep(1000);
                }
            }//一加入的client補滿
        };
        //clientsTimer.start();
        maze = new Maze(this, 16, 12);
        maze.newMap();
        playersImageIndex = new int[]{-1, -1, -1, -1, -1};
        playersName = new String[5];
        state = State.WAIT_JOIN;
    }

    public boolean startGame() {
        if (numberOfPlayer >= 2) {
            //gameThread.send(, Client.GAME_PORT);
            for (int i = 0; i < numberOfPlayer; i++) {
                maze.addPlayer(playersImageIndex[i]);
            }
            maze.makeItems();
            gameThread.send(Packet.START_GAME.setData(maze.toXMLString()), Client.GAME_PORT);
            state = State.PROCEEDING;
            return true;
        }//*/
        return false;
    }

    public void receive(MCT.Type type, Packet packet) {
        switch (type) {
            case LIST:
                receiceFromListGroup(packet);
                break;
            case GAME:
                receiceFromGameGroup(packet);
                break;
        }
    }

    private void receiceFromListGroup(Packet packet) {
        switch (packet) {
            case SEARCH_GAME:
                listThread.send(Packet.REPAY_SEARCH_GAME.setData(state + Packet.SS + gameGroupAddress + Packet.SS + gameName + Packet.SS + numberOfPlayer), LIST_PORT);
                break;
            case REPAY_SEARCH_GAME:
                String[] dataArray = packet.getData().split(Packet.SS);
                String addr = dataArray[1];
                int recGameNum = Integer.valueOf(addr.substring(addr.lastIndexOf(".") + 1));
                usedGroupNumber[recGameNum] = true;
                break;
        }
    }

    private void receiceFromGameGroup(Packet packet) {
        int recid = -1;
        String data = packet.getData();
        switch (packet) {
            case JOIN_ASK:
                recid = Integer.valueOf(data.split(Packet.SS)[0]);
                int realID = -1;
                if (numberOfPlayer < 5) {
                    synchronized (clientsLiveingTime) {
                        for (int i = 0; i < clientsLiveingTime.length; i++) {
                            if (clientsLiveingTime[i] == 0) {
                                realID = i;
                                break;
                            }
                        }
                        if (realID == -1) {
                            gameThread.send(Packet.REPAY_JOIN_DECLINE.setData(recid + ""), Client.GAME_PORT);
                        } else {
                            gameThread.send(Packet.REPAY_JOIN_ASK.setData(recid + Packet.SS + realID + Packet.SS + gameName), Client.GAME_PORT);
                            numberOfPlayer++;
                            playersImageIndex[realID] = 0;
                            playersName[realID] = data.split(Packet.SS)[1];
                            clientsLiveingTime[realID] = 6;
                        }
                    }
                } else {
                    gameThread.send(Packet.REPAY_JOIN_DECLINE.setData(recid + ""), Client.GAME_PORT);
                }
                break;
            case IM_LIEVING:
                recid = Integer.valueOf(data);
                if (recid != -1) {
                    synchronized (clientsLiveingTime) {
                        clientsLiveingTime[recid] = 6;
                    }
                }
                break;
            case SYNC_PLAYER_INFO:
                recid = Integer.valueOf(data.split(Packet.SS)[0]);
                String cliName = data.split(Packet.SS)[1];
                int imageIdx = Integer.valueOf(data.split(Packet.SS)[2]);
                playersImageIndex[recid] = imageIdx;
                playersName[recid] = cliName;
                break;
            case PLAYER_MOVE_ASK:
                recid = Integer.valueOf(data.split(Packet.SS)[0]);
                int mxp = Integer.valueOf(data.split(Packet.SS)[1]);
                int myp = Integer.valueOf(data.split(Packet.SS)[2]);
                Player p = maze.getPlayer(recid);
                if (maze.movePlayer(mxp, myp, recid)) {
                    gameThread.send(Packet.SYNC_PLAYER.setData(p.toXMLString()), Client.GAME_PORT);
                    if(maze.chechExit(p)) {
                        gameThread.send(Packet.GAME_FINISH.setData(p.id + Packet.SS + playersName[recid]), Client.GAME_PORT);
                    }
                }
                break;
            case EXIT_GAME:
                recid = Integer.valueOf(data.split(Packet.SS)[0]);
                maze.removePlayer(recid);
                //sync
                break;
            case LEAVE_GROUP:
                String recs[] = data.split(Packet.SS);
                recid = Integer.valueOf(recs[0]);
                gameThread.send(Packet.SYNC_PLAYER_INFO.setData(recid + Packet.SS + "" + Packet.SS + "-1"), Client.GAME_PORT);
                synchronized (clientsLiveingTime) {
                    clientsLiveingTime[recid] = 0;
                }
                numberOfPlayer--;
                break;
        }
    }

    public void syncPlayer(String xml) {
        gameThread.send(Packet.SYNC_PLAYER.setData(xml), Client.GAME_PORT);
    }

    public void syncItem(String xml) {
        gameThread.send(Packet.SYNC_ITEM.setData(xml), Client.GAME_PORT);
    }

    public void syncExit(String xml) {
        if (xml != null) {
            gameThread.send(Packet.SYNC_EXIT.setData(xml), Client.GAME_PORT);
        } else {
            gameThread.send(Packet.REMOVE_EXIT, Client.GAME_PORT);
        }
    }

    public String getGameGroupAddress() {
        return gameGroupAddress;
    }

    public void close() {
        synchronized (listThread) {
            if (listThread != null) {
                listThread.halt();
            }
        }
        synchronized (gameThread) {
            if (gameThread != null) {
                clientsTimer.interrupt();
                gameThread.send(Packet.LEAVE_GROUP.setData("-1" + Packet.SS + gameGroupAddress), Client.GAME_PORT);
                gameThread.halt();
            }
        }
    }
}
