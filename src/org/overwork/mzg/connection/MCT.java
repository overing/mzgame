package org.overwork.mzg.connection;

import java.net.*;
import java.util.Arrays;

/**
 * MCT = MultiCast Thread
 * @author overing
 */
public class MCT extends Thread {

    public static enum Type {

        LIST, GAME;
    }
    /** 上層參考 */
    private Receiver receiver;
    /** MCT 主執行緒的判別符號, 參考下方 void run() 副程式 */
    private boolean running;
    /** 群播群組 */
    private InetAddress mctGroup;
    /** 群組的資料傳送 Socket (資料出入口) */
    private MulticastSocket mctSocket; //
    /** 本地綁定的port */
    private int port;
    public boolean ready;
    private Type type;

    MCT(Type gt, String groupAddress, int port, Receiver receiver) {
        this.type = gt;

        this.receiver = receiver;

        try {
            mctGroup = InetAddress.getByName(groupAddress); //設定群組為預定的 IP

            this.port = port;
            mctSocket = new MulticastSocket(port); //設定收資料的 PORT

            mctSocket.joinGroup(mctGroup); //加入群組 取得 Socket

            //System.out.println("join to group:" + groupAddress + ", use local port:" + port + "\n");

            start(); //啟動執行緒
            ready = true;
        } catch (Exception ex) {
            System.err.println("construct error");
            ex.printStackTrace();
        }
    }

    /**
     * <已完成> 送出指令(灑到群播群組上)
     * @param command 封包命令類型 參考 ConnectEnums.java 的 Packet
     * @param port 送出的目標埠號
     */
    synchronized void send(Packet command, int port) {
        //組合要送出的所有字串 id:Server的id 分隔符號$
        //轉成位元陣列
        byte[] buffer = command.toString().getBytes();

        System.out.println("MCT.send(" + port + "):" + command.toString());

        //包成Socket能送的資料封包
        DatagramPacket dp = new DatagramPacket(buffer, buffer.length, mctGroup, port);
        try {
            if (!mctSocket.isClosed()) { //如果Socket不是...
                mctSocket.send(dp); //灑封包
            }
        } catch (Exception ex) { //如果有任何例外印出整串封包字串 跟 例外的詳細
            System.err.println("send data:" + command);
            ex.printStackTrace();
        }
    }

    /**
     * <已完成> 停止主執行緒用的副程式
     */
    public void halt() {
        running = false; //將辨識符號false
        send(Packet.MCT_CLOSE, port);
    }

    /**
     * <已完成> 主執行緒<p>
     * 每圈負責從socket聽, 收廣播的封包
     */
    @Override
    public void run() {
        byte[] buffer = new byte[32767]; //資料緩衝
        DatagramPacket dp = new DatagramPacket(buffer, buffer.length); //socket封包
        String alldata = null; //解析後的封包(我們定的格式)
        Packet packet;

        running = true; //設定主執行緒判別符號
        try {
            while (running) { //如果判斷符號還是true就繼續
                Arrays.fill(buffer, (byte) 0);
                mctSocket.receive(dp); //收廣播封包

                alldata = new String(buffer).trim(); //將緩衝的資料轉成我們的封包字串

                System.out.println("MCT.rec(" + port + "):" + alldata);

                packet = Packet.fromString(alldata); //第二個轉成命令類型
                receiver.receive(type, packet); //呼叫接收封包副程式處理
            }
            //會執行到這表示 running已經是false 表示主執行緒將要停了
            mctSocket.leaveGroup(mctGroup); //退出群播群組
        } catch (Exception ex) {
            //System.err.println("rec data:" + alldata);
            ex.printStackTrace();
        }
        synchronized (mctSocket) {
            mctSocket.close(); //關閉資料出入口
        }
    }

    /**
     * <pre><已完成> 解構子
     * 當物件沒有備參考到時 系統準備gc的時候會呼叫這個副程式
     * 這裡必須適當的對占用的資源做關閉及釋放的動作
     * 這樣資源才會比較快被回收</pre>
     */
    @Override
    protected void finalize() throws Throwable {
        if (this.mctSocket != null && !this.mctSocket.isClosed()) {
            this.mctSocket.leaveGroup(mctGroup);
            this.mctSocket.close();
        }
        this.mctSocket = null;
        this.mctGroup = null;
        super.finalize();
    }
}
