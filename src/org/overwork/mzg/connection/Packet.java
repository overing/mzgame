
package org.overwork.mzg.connection;

/**
 *
 * @author overing
 */
public enum Packet {

    SEARCH_GAME,
    REPAY_SEARCH_GAME, // data = [狀態&遊戲群組&人數]
    JOIN_ASK, // date = [ID&name]
    REPAY_JOIN_ASK, // date = [隨機ID&REAL ID&gameName]
    REPAY_JOIN_DECLINE, // date = [隨機ID]
    SYNC_PLAYER_INFO, // data = [ID&PlayerName&選圖序號]
    LEAVE_GROUP, // date = [ID&group]
    IM_LIEVING, // date = [ID]
    START_GAME, // data = [maze.toxml]
    PLAYER_MOVE_ASK, // data = [ID&mx&my]
    SYNC_PLAYER, // data = [player.toxml]
    SYNC_ITEM, // data = [item.toxml]
    SYNC_EXIT, // data = [exit.toxml]
    REMOVE_EXIT,
    GAME_FINISH, // data = [winnerID & winnerName]
    EXIT_GAME, // date = [ID]
    MCT_CLOSE;
    
    private String data = null;

    /** 夾帶資料串用的分隔符號 (不可以使用 '.' 字元) */
    public static final String SS = ";";

    public String getData() {
        return data;
    }

    /**
     * 設定封包所夾帶的資料
     * @param data 夾帶的資料
     * @return 已夾帶的資料封包自身
     */
    public Packet setData(String data) {
        this.data = data;
        return this;
    }

    /**
     * 將封包物件轉成字串
     * @return 字串
     */
    public String toString() {
        //System.out.println("pkt.tS:" + super.name() + (data != null && !data.isEmpty() ? Packet.SS + data : ""));
        return super.name() + (data != null && !data.isEmpty() ? Packet.SS + data : "");
    }

    /**
     * 將字串資料還原成封包物件
     * @param str 字串資料
     * @return 封包物件
     */
    public static Packet fromString(String str) {
        //System.out.println("pkt.fS:" + str);
        String[] stra = str.split(Packet.SS);
        Packet rval = Packet.valueOf(stra[0]);
        String data = (stra.length >= 2 && !stra[1].isEmpty()) ? str.substring(str.indexOf(Packet.SS) + 1) : null;
        return rval.setData(data);
    }
}
