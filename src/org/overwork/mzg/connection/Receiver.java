package org.overwork.mzg.connection;

public interface Receiver {

    public void receive(MCT.Type type, Packet packet);
}
