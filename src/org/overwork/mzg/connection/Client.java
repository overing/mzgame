package org.overwork.mzg.connection;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Vector;
import org.overwork.mzg.MainFrame;
import org.overwork.mzg.connection.MCT.Type;
import org.overwork.mzg.maze.Maze;

/**
 *
 * @author overing
 */
public class Client implements Receiver {

    public static enum State {

        LIST,
        WAIT_START,
        PROCEEDING;

        public String toString() {
            return this.name();
        }
    }
    public static final int GAME_PORT = 8080;
    public static final int HEARTBEAT = 2000;
    private State state;
    private MCT listThread, gameThread;
    private String gameGroupAddress;
    private Vector<String> gameList = new Vector<String>();
    private int id;
    private int waitTime;
    private MainFrame mfRef;
    private SayImLieving sayImLieving;
    private String gameName;
    private String name;
    private Maze maze;

    public Client(MainFrame mfRef) {
        this.mfRef = mfRef;
        name = mfRef.getClientName();
        id = makeRandomID();
        listThread = new MCT(MCT.Type.LIST, Server.LIST_GROUP_ADDRESS, Server.LIST_PORT, this);
        while (!listThread.ready) {
        }
    }

    public Client(MainFrame mfRef, String gameGroupAddress) {
        this.mfRef = mfRef;
        name = mfRef.getClientName();
        joinGame(gameGroupAddress);
    }

    private static int makeRandomID() {
        String hostname, username;
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            hostname = String.valueOf(Math.random());
        }
        username = System.getProperty("user.name");
        return 10 + Math.abs((hostname + username + Math.random()).hashCode());
    }

    public void sendToGameGroup(Packet command, int port) {
        gameThread.send(command, port);
    }

    public Vector<String> searchGame() {
        state = State.LIST;
        listThread.send(Packet.SEARCH_GAME, Server.LIST_PORT);
        MainFrame.sleep(500);
        return gameList;
    }

    public boolean joinGame(String groupAddress) {
        gameThread = new MCT(MCT.Type.GAME, groupAddress, GAME_PORT, this);
        while (!gameThread.ready) {
        }
        gameGroupAddress = groupAddress;
        waitTime = 6;
        gameThread.send(Packet.JOIN_ASK.setData(id + Packet.SS + name), Server.GAME_PORT);
        while (waitTime > 0) {
            MainFrame.sleep(500);
            waitTime--;
        }

        if (id < 10) {
            state = State.WAIT_START;
            if (listThread != null) {
                listThread.halt();
            }
            gameThread.send(Packet.SYNC_PLAYER_INFO.setData(id + Packet.SS + name + Packet.SS + 0), Server.GAME_PORT);
            gameThread.send(Packet.SYNC_PLAYER_INFO.setData(id + Packet.SS + name + Packet.SS + 0), GAME_PORT);
            sayImLieving = new SayImLieving(this);
            return true;
        } else {
            gameThread.halt();
            return false;
        }
    }

    public MCT getGameThread() {
        return gameThread;
    }

    public String getGameName() {
        return gameName;
    }

    public Maze getMaze() {
        return maze;
    }

    public int getId() {
        return id;
    }

    @Override
    public void receive(Type type, Packet packet) {
        switch (type) {
            case LIST:
                receiceFromListGroup(packet);
                break;
            case GAME:
                receiceFromGameGroup(packet);
                break;
        }
    }

    private void receiceFromListGroup(Packet packet) {
        switch (packet) {
            case REPAY_SEARCH_GAME:
                String[] dataArray = packet.getData().split(Packet.SS);
                String serState = dataArray[0];
                String nOP = dataArray[3];
                if (serState.equals(Server.State.WAIT_JOIN.toString()) && Integer.valueOf(nOP) < 5) {
                    String recGameName = dataArray[2];
                    String recGameGroupAddress = dataArray[1];
                    gameList.add(recGameGroupAddress + Packet.SS + recGameName);
                }
                break;
        }
    }

    private void receiceFromGameGroup(Packet packet) {
        String data = packet.getData();
        int recID;
        switch (packet) {
            case REPAY_JOIN_ASK:
                recID = Integer.valueOf(data.split(Packet.SS)[0]);
                if (recID == id) {
                    id = Integer.valueOf(data.split(Packet.SS)[1]);
                    gameName = data.split(Packet.SS)[2];
                    mfRef.c_SetDrawArray(id);
                    waitTime = 0;
                } else {
                    gameThread.send(Packet.SYNC_PLAYER_INFO.setData(id + Packet.SS + name + Packet.SS + mfRef.c_GetImageIndex(id)), GAME_PORT);
                }
                break;
            case REPAY_JOIN_DECLINE:
                recID = Integer.valueOf(data.split(Packet.SS)[0]);
                if (recID == id) {
                    waitTime = 0;
                }
                break;
            case SYNC_PLAYER_INFO:
                recID = Integer.valueOf(data.split(Packet.SS)[0]);
                String recName = data.split(Packet.SS)[1];
                int imageIdx = Integer.valueOf(data.split(Packet.SS)[2]);
                mfRef.c_UpdatePlayerInfo(recID, recName, imageIdx);
                break;
            case START_GAME:
                if (state == State.WAIT_START) {
                    maze = Maze.fromXMLString(data);
                    state = State.PROCEEDING;
                    mfRef.c_StartMultiGame();
                }
                break;
            case SYNC_PLAYER:
                maze.updatePlayer(data);
                break;
            case SYNC_ITEM:
                if (maze != null) {
                    maze.updateItem(data);
                }
                break;
            case SYNC_EXIT:
                maze.updateExit(data);
                break;
            case REMOVE_EXIT:
                maze.updateExit(null);
                break;
            case GAME_FINISH:
                mfRef.c_FinishMultiGame(data);
                break;
            case LEAVE_GROUP:
                String recGroupAddress = data.split(Packet.SS)[1];
                if (gameGroupAddress.equals(recGroupAddress)) {
                    mfRef.c_BreakMULTI_MODE_SELECT();
                }
                break;
        }
    }

    public void close() {
        if (listThread != null) {
            listThread.halt();
            listThread = null;
        }
        if (gameThread != null) {
            gameThread.send(Packet.LEAVE_GROUP.setData(id + Packet.SS + gameGroupAddress), Server.GAME_PORT);
            gameThread.halt();
            gameThread = null;
        }
        if (this.sayImLieving != null) {
            sayImLieving.interrupt();
            sayImLieving = null;
        }
    }
}

class SayImLieving extends Thread {

    private Client c;

    public SayImLieving(Client c) {
        this.c = c;
        start();
    }

    public void run() {
        while (c.getGameThread() != null) {
            c.getGameThread().send(Packet.IM_LIEVING.setData(c.getId() + ""), Server.GAME_PORT);
            MainFrame.sleep(Client.HEARTBEAT);
        }
    }
}
